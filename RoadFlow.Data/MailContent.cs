﻿using RoadFlow.Mapper;
using System;

namespace RoadFlow.Data
{
  public class MailContent
  {
    public RoadFlow.Model.MailContent Get(Guid id)
    {
      using (DataContext dataContext = new DataContext())
        return dataContext.Find<RoadFlow.Model.MailContent>((object) id);
    }

    public int Add(RoadFlow.Model.MailContent mailContent)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Add<RoadFlow.Model.MailContent>(mailContent);
        return dataContext.SaveChanges();
      }
    }

    public int Update(RoadFlow.Model.MailContent mailContent)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Update<RoadFlow.Model.MailContent>(mailContent);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(RoadFlow.Model.MailContent mailContent)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Remove<RoadFlow.Model.MailContent>(mailContent);
        return dataContext.SaveChanges();
      }
    }
  }
}
