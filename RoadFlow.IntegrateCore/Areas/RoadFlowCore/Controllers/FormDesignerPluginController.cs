﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RoadFlow.Utility;
using System.IO;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FormDesignerPluginController : Controller
    {
        [Validate(CheckApp = false)]
        public IActionResult Attribute()
        {
            ViewData["dbconnOptions"] = new Business.DbConnection().GetOptions();
            ViewData["formTypeOptions"] = new Business.Dictionary().GetOptionsByCode("system_applibrarytype_form");
            return View(); 
        }

        public IActionResult Events()
        {
            return View();
        }

        public IActionResult Text()
        {
            return View();
        }

        public IActionResult Textarea()
        {
            return View();
        }

        public IActionResult Select()
        {
            return View();
        }

        public IActionResult Radio()
        {
            return View();
        }

        public IActionResult Checkbox()
        {
            return View();
        }

        public IActionResult Hidden()
        {
            return View();
        }

        public IActionResult Button()
        {
            return View();
        }

        public IActionResult Html()
        {
            return View();
        }

        public IActionResult Label()
        {
            return View();
        }

        public IActionResult Datetime()
        {
            return View();
        }

        public IActionResult Organize()
        {
            return View();
        }

        public IActionResult Lrselect()
        {
            return View();
        }

        public IActionResult SerialNumber()
        {
            return View();
        }

        public IActionResult Files()
        {
            return View();
        }

        public IActionResult SubTable()
        {
            ViewData["appid"] = Request.Querys("appid");
            ViewData["formTypes"] = new Business.Dictionary().GetOptionsByCode("system_applibrarytype_form");
            return View();
        }

        public IActionResult SubtableSet()
        {
            ViewData["eid"] = Request.Querys("eid");
            ViewData["dbconn"] = Request.Querys("dbconn");
            ViewData["secondtable"] = Request.Querys("secondtable");
            ViewData["connOptions"] = new Business.DbConnection().GetOptions(Request.Querys("dbconn"));
            return View();
        }

        [Validate(CheckApp = false)]
        public string SaveForm()
        {
            string attr = Request.Forms("attr");
            string events = Request.Forms("event");
            string subtable = Request.Forms("subtable");
            string html = Request.Forms("html");

            JObject jObject = null;
            try
            {
                jObject = JObject.Parse(attr);
            }
            catch
            {
                return "属性JSON解析错误!";
            }
            string id = jObject.Value<string>("id");
            string name = jObject.Value<string>("name");
            string formType = jObject.Value<string>("formType");
            if (!id.IsGuid(out Guid guid))
            {
                return "表单ID不能为空!";
            }
            if (name.IsNullOrWhiteSpace())
            {
                return "表单名称为空!";
            }
            if (!formType.IsGuid(out Guid typeId))
            {
                return "表单分类不能为空!";
            }
            Business.Form form = new Business.Form();
            Model.Form formModel = form.Get(guid);
            bool isAdd = false;
            if (null == formModel)
            {
                formModel = new Model.Form
                {
                    Id = guid,
                    Status = 0,
                    CreateDate = DateExtensions.Now,
                    CreateUserId = Current.UserId,
                    CreateUserName = Current.UserName
                };
                isAdd = true;
            }
            formModel.Name = name.Trim();
            formModel.FormType = typeId;
            formModel.EventJSON = events;
            formModel.SubtableJSON = subtable;
            formModel.attribute = attr;
            formModel.Html = html;
            formModel.EditDate = DateExtensions.Now;
            int i = isAdd ? form.Add(formModel) : form.Update(formModel);
            Business.Log.Add("保存了表单-" + name, formModel.ToString(), Business.Log.Type.流程管理);
            return "保存成功!";
        }

        private readonly IHostingEnvironment _hostingEnvironment;
        public FormDesignerPluginController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        [Validate]
        public string PublishForm()
        {
            string attr = Request.Forms("attr");
            string events = Request.Forms("event");
            string subtable = Request.Forms("subtable");
            string html = Request.Forms("html");
            string formHtml = Request.Forms("formHtml");

            JObject jObject = null;
            try
            {
                jObject = JObject.Parse(attr);
            }
            catch
            {
                return "属性JSON解析错误!";
            }
            string id = jObject.Value<string>("id");
            string name = jObject.Value<string>("name");
            string formType = jObject.Value<string>("formType");
            if (!id.IsGuid(out Guid guid))
            {
                return "表单ID不能为空!";
            }
            if (name.IsNullOrWhiteSpace())
            {
                return "表单名称为空!";
            }
            if (!formType.IsGuid(out Guid typeId))
            {
                return "表单分类不能为空!";
            }

            #region 保存数据表
            Business.Form form = new Business.Form();
            Model.Form formModel = form.Get(guid);
            bool isAdd = false;
            if (null == formModel)
            {
                formModel = new Model.Form
                {
                    Id = guid,
                    Status = 0,
                    CreateDate = DateExtensions.Now,
                    CreateUserId = Current.UserId,
                    CreateUserName = Current.UserName
                };
                isAdd = true;
            }
            formModel.Name = name.Trim();
            formModel.FormType = typeId;
            formModel.EventJSON = events;
            formModel.SubtableJSON = subtable;
            formModel.attribute = attr;
            formModel.Html = html;
            formModel.EditDate = DateExtensions.Now;
            formModel.Status = 1;
            int i = isAdd ? form.Add(formModel) : form.Update(formModel);
            #endregion

            #region 写入文件
            string webRootPath = _hostingEnvironment.WebRootPath;
            string file = webRootPath + "/RoadFlowResources/scripts/formDesigner/form/" + formModel.Id + ".cshtml";
            Stream stream = System.IO.File.Open(file, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
            stream.SetLength(0);
            StreamWriter sw = new StreamWriter(stream, System.Text.Encoding.UTF8);
            sw.Write(formHtml);
            sw.Close();
            stream.Close();
            #endregion

            #region 写入应用程序库
            Business.AppLibrary appLibrary = new Business.AppLibrary();
            var appModel = appLibrary.GetByCode(formModel.Id.ToString());
            bool add = false;
            if (null == appModel)
            {
                add = true;
                appModel = new Model.AppLibrary
                {
                    Id = Guid.NewGuid(),
                    Code = formModel.Id.ToString()
                };
            }
            appModel.Title = formModel.Name;
            appModel.Type = formModel.FormType;
            appModel.Address = formModel.Id.ToString() + ".cshtml";
            int j = add ? appLibrary.Add(appModel) : appLibrary.Update(appModel);
            #endregion

            Business.Log.Add("发布了表单-" + name, formModel.ToString(), Business.Log.Type.流程管理, others: formHtml);
            return "发布成功!";
        }
    }
}