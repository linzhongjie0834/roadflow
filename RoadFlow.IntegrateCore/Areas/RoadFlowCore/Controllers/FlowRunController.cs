﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FlowRunController : Controller
    {
        [Validate(CheckApp = false)]
        public IActionResult Index()
        {
            string flowid = Request.Querys("flowid");
            string stepid = Request.Querys("stepid");
            string taskid = Request.Querys("taskid");
            string groupid = Request.Querys("groupid");
            string instanceid = Request.Querys("instanceid");
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string display = Request.Querys("display");
            string showtoolbar = Request.Querys("showtoolbar");//是否显示工具栏，查看表单的时候，有时不需要显示。
            int rf_appopenmodel = Request.Querys("string rf_appopenmodel").ToInt(0);//窗口打开方式 0tab, 1,2弹出层 3,4,5弹出窗口

            if (display.IsNullOrEmpty())
            {
                display = "0";
            }
            if (!flowid.IsGuid(out Guid flowId))
            {
                return new ContentResult() { Content = "流程ID错误!" };
            }
            Business.Flow flow = new Business.Flow();
            Model.FlowRun flowRunModel = flow.GetFlowRunModel(flowId);
            if (null == flowRunModel)
            {
                return new ContentResult() { Content = "未找到流程运行时实体!" };
            }
            Guid stepId = stepid.IsGuid(out Guid stepGuid) ? stepGuid : flowRunModel.FirstStepId;
            if (flowRunModel.Status != 1 && stepGuid == flowRunModel.FirstStepId)
            {
                return new ContentResult() { Content = "流程" + (flowRunModel.Status == 0 ? "未发布" : flowRunModel.Status == 2 ? "已卸载" : "已删除") + ",不能发起新的流程实例!" };
            }

            Business.FlowTask flowTask = new Business.FlowTask();
            Model.FlowTask flowTaskModel = null;
            if (taskid.IsGuid(out Guid taskGuid))
            {
                flowTaskModel = flowTask.Get(taskGuid);
            }

            bool isAddWrite = null != flowTaskModel && flowTaskModel.TaskType.In(6, 7, 8);//是否是加签
            Model.FlowRunModel.Step stepModel = flowRunModel.Steps.Find(p => p.Id == stepId);
            if (null == stepModel)
            {
                return new ContentResult() { Content = "未找到当前步骤运行时实体!" };
            }

            //检查并发处理
            if (1 == stepModel.StepBase.ConcurrentModel && null != flowTaskModel && flowTaskModel.TaskType != 5)
            {
                var executeTask = flowTask.GetListByGroupId(flowTaskModel.GroupId).Find(p => p.TaskType != 5 && p.Status == 1 && p.ReceiveId != flowTaskModel.ReceiveId);
                if (null != executeTask)
                {
                    string closeScript = string.Empty;
                    if (0 == rf_appopenmodel)
                    {
                        closeScript = "top.mainTab.closeTab();";
                    }
                    else if (rf_appopenmodel.In(1, 2))
                    {
                        closeScript = "top.mainDialog.close();";
                    }
                    else if (rf_appopenmodel.In(3, 4, 5))
                    {
                        closeScript = "window.close()";
                    }
                    return new ContentResult() { Content = "<script>alert('当前任务正由" + executeTask.ReceiveName + "处理中,请等待!');"+ closeScript + "</script>", ContentType = "text/html" };
                }
            }

            //更新任务状态为处理中
            if (null != flowTaskModel && flowTaskModel.Status == 0)
            {
                flowTaskModel.Status = 1;
                flowTaskModel.ExecuteType = 1;
                if (!flowTaskModel.OpenTime.HasValue)//更新打开时间
                {
                    flowTaskModel.OpenTime = DateExtensions.Now;
                }
                flowTask.Update(flowTaskModel);
            }

            string query = "flowid=" + flowid + "&taskid=" + taskid + "&groupid=" + groupid + "&stepid=" + stepId + "&instanceid="
                + instanceid + "&appid=" + appid + "&tabid=" + tabid + "&dilplay=" + display + "&showtoolbar=" + showtoolbar 
                + "&rf_appopenmodel=" + Request.Querys("rf_appopenmodel");

            #region 组织流程按钮
            List<Model.FlowRunModel.StepButton> stepButtons = new List<Model.FlowRunModel.StepButton>();
            List<Model.FlowButton> flowButtons = new List<Model.FlowButton>();
            Business.FlowButton flowButton = new Business.FlowButton();
            if ("1".Equals(display))//如果是查看则显示打印和过程查看按钮
            {
                if (!"0".Equals(showtoolbar))
                {
                    stepButtons.Add(new Model.FlowRunModel.StepButton()
                    {
                        Id = "cadd9f81-2b8c-479b-a7f1-3cec775768fa".ToGuid(),
                        Sort = 0
                    });
                    stepButtons.Add(new Model.FlowRunModel.StepButton()
                    {
                        Id = "d9511329-a03e-4af2-84e5-73beda0d3f42".ToGuid(),
                        Sort = 1
                    });
                }
            }
            else if (null != flowTaskModel && flowTaskModel.TaskType == 5)//如果是抄送任务，只显示已阅知和打印按钮
            {
                stepButtons.Add(new Model.FlowRunModel.StepButton()
                {
                    Id = "cadd9f81-2b8c-479b-a7f1-3cec775768fa".ToGuid(),
                    Sort = 0
                });
                stepButtons.Add(new Model.FlowRunModel.StepButton()
                {
                    Id = "aa27eab5-66f0-4cde-958f-1f28da4b4392".ToGuid(),
                    Sort = 1
                });
            }
            else
            {
                stepButtons.AddRange(stepModel.StepButtons);
            }
            //如果是子流程要显示查看主流程表单按钮
            if (null != flowTaskModel && flowTaskModel.OtherType == 1)
            {
                stepButtons.Add(new Model.FlowRunModel.StepButton() {
                    Id = "1673ff03-48c6-465a-9caa-46b776c932a9".ToGuid(),
                    Sort = stepButtons.Count + 5
                });
            }
            foreach (var stepButton in stepButtons)
            {
                if (stepButton.Id.IsEmptyGuid())
                {
                    var flowButtonModel = new Model.FlowButton() { Id = Guid.Empty, Title = "" };
                    flowButtons.Add(flowButtonModel);
                }
                else
                {
                    var flowButtonModel = flowButton.Get(stepButton.Id).Clone();
                    if (null != flowButtonModel)
                    {
                        flowButtonModel.Title = stepButton.ShowTitle.IsNullOrWhiteSpace() ? flowButtonModel.Title : stepButton.ShowTitle;
                        flowButtonModel.Sort = stepButton.Sort;
                        flowButtons.Add(flowButtonModel);
                    }
                }
            }
            #endregion

            #region 得到表单地址
            Model.FlowRunModel.StepForm stepFormModel = stepModel.StepForm;
            string formUrl = string.Empty;
            bool isCustomeForm = false;//是否是自定义表单
            if (null != stepFormModel)
            {
                if (!stepFormModel.Id.IsEmptyGuid())
                {
                    var applibraryModel = new Business.AppLibrary().Get(stepFormModel.Id);
                    if (null != applibraryModel)
                    {
                        isCustomeForm = applibraryModel.Code.IsNullOrWhiteSpace();
                        if (isCustomeForm)
                        {
                            formUrl = new Business.Menu().GetAddress(applibraryModel.Address, query);
                        }
                        else
                        {
                            formUrl = Url.Content("~/wwwroot/RoadFlowResources/scripts/formDesigner/form/" + applibraryModel.Address);
                        }
                    }
                }
            }
            #endregion

            #region 签章图片
            var user = Current.User;
            string signPath = Current.WebRootPath + "/RoadFlowResources/images/userSigns/" + user.Id.ToString("N");
            System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(signPath);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            if (!System.IO.File.Exists(signPath+ "/default.png"))
            {
                var img = new Business.User().CreateSignImage(user.Name);
                img.Save(signPath+ "/default.png", System.Drawing.Imaging.ImageFormat.Png);
            }
            string signSrc = Url.Content("~/RoadFlowResources/images/userSigns/" + user.Id.ToString("N") + "/default.png");
            #endregion

            ViewData["tabId"] = tabid;
            ViewData["appId"] = appid;
            ViewData["instanceId"] = instanceid;
            ViewData["display"] = display;
            ViewData["formUrl"] = formUrl;
            ViewData["query"] = query;
            ViewData["isCustomeForm"] = isCustomeForm ? "1" : "0";//是否是自定义表单
            ViewData["isDebug"] = flowRunModel.Debug;
            ViewData["signType"] = flowTaskModel != null && flowTaskModel.TaskType == 5 ? 0 : stepModel.SignatureType;//如果是抄送任务不显示意见栏
            ViewData["flowType"] = stepModel.StepBase.FlowType;
            ViewData["isArchives"] = stepModel.Archives;
            ViewData["flowButtons"] = flowButtons;
            ViewData["signSrc"] = signSrc;//签章图片地址
            ViewData["request"] = Request;
            ViewData["showComment"] = stepModel.CommentDisplay;//是否显示历史意见
            ViewData["commentOptions"] = new Business.FlowComment().GetOptionsByUserId(user.Id);//默认处理意见选项
            return View();
        }

        /// <summary>
        /// 发送
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult FlowSend()
        {
            string flowid = Request.Querys("flowid");
            string taskid = Request.Querys("taskid");
            string stepid = Request.Querys("stepid");
            string groupid = Request.Querys("groupid");
            string instanceid = Request.Querys("instanceid");
            string freedomsend = Request.Querys("freedomsend");
            if (instanceid.IsNullOrWhiteSpace())
            {
                instanceid = Request.Querys("instanceid1");
            }

            if (!flowid.IsGuid(out Guid flowId))
            {
                return new ContentResult() { Content = "流程ID错误!" };
            }
            if (!stepid.IsGuid(out Guid stepId))
            {
                return new ContentResult() { Content = "步骤ID错误!" };
            }
            var flowRunModel = new Business.Flow().GetFlowRunModel(flowId);
            if (null == flowRunModel)
            {
                return new ContentResult() { Content = "未找到流程运行时!" };
            }
            var (html, message, sendSteps) = new Business.FlowTask().GetNextSteps(flowRunModel, stepId, groupid.ToGuid(), taskid.ToGuid(), instanceid, Current.UserId, "1".Equals(freedomsend));
            var stepModel = flowRunModel.Steps.Find(p => p.Id == stepId);
            ViewData["nextSteps"] = html;
            ViewData["openerId"] = Request.Querys("openerid");
            ViewData["freedomSend"] = freedomsend;
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["iframeId"] = Request.Querys("iframeid");
            ViewData["nextStepCount"] = sendSteps.Count;
            ViewData["autoConfirm"] = null != stepModel ? stepModel.StepBase.AutoConfirm : 0;//是否自动确认
            ViewData["isAddWrite"] = "0";
            return View();
        }

        /// <summary>
        /// 退回
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult FlowBack()
        {
            string flowid = Request.Querys("flowid");
            string taskid = Request.Querys("taskid");
            string stepid = Request.Querys("stepid");
            string groupid = Request.Querys("groupid");
            string instanceid = Request.Querys("instanceid");
            if (instanceid.IsNullOrWhiteSpace())
            {
                instanceid = Request.Querys("instanceid1");
            }
            if (!flowid.IsGuid(out Guid flowId))
            {
                return new ContentResult() { Content = "流程ID错误" };
            }
            if (!stepid.IsGuid(out Guid stepId))
            {
                return new ContentResult() { Content = "步骤ID错误" };
            }
            var (html, message, sendSteps) = new Business.FlowTask().GetBackSteps(flowId, stepId, groupid.ToGuid(), taskid.ToGuid(), instanceid, Current.UserId);
            ViewData["backSteps"] = html;
            ViewData["message"] = message;
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["iframeId"] = Request.Querys("iframeid");
            ViewData["openerId"] = Request.Querys("openerid");
            return View();
        }

        /// <summary>
        /// 转交
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult FlowRedirect()
        {
            ViewData["stepId"] = Request.Querys("stepid");
            ViewData["openerId"] = Request.Querys("openerid");
            return View();
        }

        /// <summary>
        /// 抄送
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult FlowCopyFor()
        {
            ViewData["taskId"] = Request.Querys("taskid");
            return View();
        }

        /// <summary>
        /// 保存抄送
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        [ValidateAntiForgeryToken]
        public string FlowCopyForSave()
        {
            string taskid = Request.Forms("taskid");
            string user = Request.Forms("user");
            var users = new Business.Organize().GetAllUsers(user);
            if (users.Count == 0)
            {
                return "没有接收人!";
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var taskModel = flowTask.Get(taskid.ToGuid());
            string msg = new Business.FlowTask().CopyFor(taskModel, users);
            return "1".Equals(msg) ? "抄送成功!" : msg;
        }

        /// <summary>
        /// 签章
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Sign()
        {
            ViewData["openerId"] = Request.Querys("openerid");
            ViewData["queryString"] = Request.UrlQuery();
            return View();
        }
        /// <summary>
        /// 验证签章
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        [ValidateAntiForgeryToken]
        public string SignCheck()
        {
            string pass = Request.Forms("pass");
            if (pass.IsNullOrWhiteSpace())
            {
                return "密码不能为空!";
            }
            var user = Current.User;
            if (null == user)
            {
                return "未找到您的用户信息!";
            }
            if (!user.Password.Equals(new Business.User().GetMD5Password(user.Id, pass.Trim())))
            {
                return "密码错误!";
            }
            return "1";
        }

        /// <summary>
        /// 显示流程图
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult ShowDesign()
        {
            ViewData["flowid"] = Request.Querys("flowid");
            ViewData["tabid"] = Request.Querys("tabid");
            ViewData["appid"] = Request.Querys("appid");
            return View();
        }

        /// <summary>
        /// 打印
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Print()
        {
            string flowid = Request.Querys("flowid");
            string stepid = Request.Querys("stepid");
            string taskid = Request.Querys("taskid");
            string groupid = Request.Querys("groupid");
            string instanceid = Request.Querys("instanceid");
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string showarchive = Request.Querys("showarchive");

            if (instanceid.IsNullOrWhiteSpace())
            {
                instanceid = Request.Querys("instanceid1");
            }

            if (!flowid.IsGuid(out Guid flowId))
            {
                return new ContentResult() { Content = "流程ID错误!" };
            }
            if (!taskid.IsGuid(out Guid taskId))
            {
                return new ContentResult() { Content = "未找到当前任务，请先保存再打印!" };
            }
            Business.Flow flow = new Business.Flow();
            var flowRunModel = flow.GetFlowRunModel(flowId);
            if (null == flowRunModel)
            {
                return new ContentResult() { Content = "未找到当前流程运行时实体!" };
            }
            if (!stepid.IsGuid(out Guid stepId))
            {
                return new ContentResult() { Content = "未找到当前步骤，请先保存再打印!" };
            }
            var stepModel = flowRunModel.Steps.Find(p => p.Id == stepId);
            if (null == stepModel)
            {
                return new ContentResult() { Content = "未找到当前步骤运行时实体!" };
            }

            string query = "flowid=" + flowid + "&taskid=" + taskid + "&groupid=" + groupid + "&stepid=" + stepId + "&instanceid="
                + instanceid + "&appid=" + appid + "&tabid=" + tabid + "&dilplay=1";

            Model.FlowRunModel.StepForm stepFormModel = stepModel.StepForm;
            string formUrl = string.Empty;
            bool isCustomeForm = false;//是否是自定义表单
            if (null != stepFormModel)
            {
                if (!stepFormModel.Id.IsEmptyGuid())
                {
                    var applibraryModel = new Business.AppLibrary().Get(stepFormModel.Id);
                    if (null != applibraryModel)
                    {
                        isCustomeForm = applibraryModel.Code.IsNullOrWhiteSpace();
                        if (isCustomeForm)
                        {
                            formUrl = new Business.Menu().GetAddress(applibraryModel.Address, query);
                        }
                        else
                        {

                            formUrl = Url.Content("~/wwwroot/RoadFlowResources/scripts/formDesigner/form/" + applibraryModel.Address);
                        }
                    }
                }
            }
            string comments = string.Empty;
            if ("1".Equals(showarchive))//如果是查看归档，要查询处理意见HTML
            {
                string archiveId = Request.Querys("archiveid");
                if (archiveId.IsGuid(out Guid aid))
                {
                    comments = new Business.FlowArchive().GetArchiveComments(aid);
                }
            }
            ViewData["comments"] = comments;
            ViewData["formUrl"] = formUrl;
            ViewData["isCustomeForm"] = isCustomeForm ? "1" : "0";
            ViewData["request"] = Request;
            ViewData["showarchive"] = showarchive;
            return View();
        }

        /// <summary>
        /// 查看主流程表单
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult ShowMainForm()
        {
            Business.FlowTask flowTask = new Business.FlowTask();
            string taskid = Request.Querys("taskid");
            var currentTask = flowTask.Get(taskid.ToGuid());
            if (null == currentTask)
            {
                return new ContentResult() { Content = "未找到当前任务!" };
            }
            var tasks = flowTask.GetListBySubFlowGroupId(currentTask.GroupId);
            if (tasks.Count == 0)
            {
                return new ContentResult() { Content = "未找到当前任务的主流程任务!" };
            }
            var task = tasks.First();
            string url = "Index?flowid=" + task.FlowId + "&stepid=" + task.StepId + "&instanceid=" + task.InstanceId
                + "&taskid=" + task.Id + "&groupid=" + task.GroupId + "&appid=" + Request.Querys("appid")
                + "&display=1&showtoolbar=0&tabid=" + Request.Querys("tabid");
            return Redirect(url);
        }

        /// <summary>
        /// 加签
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult AddWrite()
        {
            ViewData["queryString"] = Request.UrlQuery();
            ViewData["rf_appopenmodel"] = Request.Querys("rf_appopenmodel");
            ViewData["openerId"] = Request.Querys("openerid");
            return View();
        }

        /// <summary>
        /// 保存加签(暂时不用,改用统一提交到Execute处理)
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public string SaveAddWrite()
        {
            int addtype = Request.Forms("addtype").ToInt(0);
            int writetype = Request.Forms("writetype").ToInt(0);
            string writeuser = Request.Forms("writeuser");
            string comments = Request.Forms("comments");
            int issign = Request.Forms("issign").ToInt(0);
            string taskId = Request.Querys("taskid");
            if (!taskId.IsGuid(out Guid tid))
            {
                return "当前任务ID错误!";
            }
            if (addtype == 0)
            {
                return "请选择加签类型!";
            }
            if (writetype == 0)
            {
                return "请选择审批方式!";
            }
            if (writeuser.IsNullOrWhiteSpace())
            {
                return "请选择接收人!";
            }
            //var result = new Business.FlowTask().AddWrite(tid, addtype, writetype, writeuser, comments, issign);
            //if (result.IsSuccess)
            //{
            //    return result.Messages;
            //}
            //else
            //{
            //    return result.Messages;
            //}
            return "";
        }

        /// <summary>
        /// 发起流程页面
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Starts()
        {
            var flows = new Business.Flow().GetStartFlows(Current.UserId);
            return View(flows);
        }

        /// <summary>
        /// 执行
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Execute()
        {
            string paramsJSON = Request.Forms("params");
            string issign = Request.Forms("issign");
            string comment = Request.Forms("comment");
            string flowid = Request.Querys("flowid");
            string instanceid = Request.Querys("instanceid");
            string taskid = Request.Querys("taskid");
            string stepid = Request.Querys("stepid");
            string groupid = Request.Querys("groupid");
            if (instanceid.IsNullOrWhiteSpace())
            {
                instanceid = Request.Forms("instanceid");
            }
            ViewData["rf_appopenmodel"] = Request.Querys("rf_appopenmodel");
            #region 参数检查
            if (paramsJSON.IsNullOrWhiteSpace())
            {
                ViewData["alertMsg"] = "执行参数为空";
                ViewData["success"] = "0";
                return View();
            }
            if (!flowid.IsGuid(out Guid flowId))
            {
                ViewData["alertMsg"] = "流程ID错误";
                ViewData["success"] = "0";
                return View();
            }
            Business.Flow flow = new Business.Flow();
            Business.FlowTask flowTask = new Business.FlowTask();
            var flowRunModel = flow.GetFlowRunModel(flowId);
            if (null == flowRunModel)
            {
                ViewData["alertMsg"] = "未找到流程运行实体";
                ViewData["success"] = "0";
                return View();
            }
            JObject paramsObject = JObject.Parse(paramsJSON);
            string opation = paramsObject.Value<string>("type");
            if (opation.IsNullOrWhiteSpace())
            {
                ViewData["alertMsg"] = "未找到要处理的类型";
                ViewData["success"] = "0";
                return View();
            }
            #endregion

            #region 组织执行参数实体
            Model.FlowRunModel.Execute executeModel = new Model.FlowRunModel.Execute();
            switch (opation)
            {
                case "freesubmit": //自由流程发送
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.FreeSubmit;
                    break;
                case "submit":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.Submit;
                    break;
                case "save":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.Save;
                    break;
                case "back":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.Back;
                    break;
                case "completed":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.Completed;
                    break;
                case "redirect":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.Redirect;
                    break;
                case "addwrite":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.AddWrite;
                    break;
                case "copyforcompleted": //抄送完成(已阅知)
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.CopyforCompleted;
                    break;
                case "taskend":
                    executeModel.ExecuteType = Model.FlowRunModel.Execute.Type.TaskEnd;
                    break;
            }
            executeModel.Comment = comment;
            executeModel.FlowId = flowId;
            executeModel.GroupId = groupid.ToGuid();
            executeModel.InstanceId = instanceid;
            executeModel.IsSign = issign.ToInt(0);
            executeModel.Note = "";
            executeModel.OtherType = 0;
            executeModel.Sender = Current.User;
            executeModel.StepId = stepid.IsGuid(out Guid stepId) ? stepId : flowRunModel.FirstStepId;
            executeModel.TaskId = taskid.ToGuid();
            executeModel.ParamsJSON = paramsJSON;

            List<(Guid stepId, List<Model.User> receiveUsers, DateTime? completedTime)> nextSteps = new List<(Guid stepId, List<Model.User> receiveUsers, DateTime? completedTime)>();
            var currentStepModel = flowRunModel.Steps.Find(p => p.Id == executeModel.StepId);
            if (null == currentStepModel)
            {
                ViewData["alertMsg"] = "未找到当前步骤!";
                ViewData["success"] = "0";
                return View();
            }
            JArray stepsArray = paramsObject.Value<JArray>("steps");
            if (null != stepsArray)
            {
                Business.Organize organize = new Business.Organize();
                Business.FlowEntrust flowEntrust = new Business.FlowEntrust();
                Business.User user = new Business.User();
                foreach (JObject stepObject in stepsArray)
                {
                    string id = stepObject.Value<string>("id");
                    string member = stepObject.Value<string>("member");
                    string completedTime = stepObject.Value<string>("completedtime");
                    if (!id.IsGuid(out Guid nextStepId))
                    {
                        continue;
                    }
                    DateTime? completedDateTime = new DateTime?();
                    if (completedTime.IsDateTime(out DateTime cTime))
                    {
                        completedDateTime = cTime;
                    }
                    if (executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.Submit ||
                        executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.Back ||
                        executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.Redirect || 
                        executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.FreeSubmit)
                    {
                        var userList = organize.GetAllUsers(member);
                        #region 判断委托
                        for (int i = 0; i < userList.Count; i++)
                        {
                            var userModel = userList[i].Clone();
                            userModel.Note = "";
                            string entrustId = flowEntrust.GetEntrustUserId(flowId, userModel);
                            if (!entrustId.IsNullOrWhiteSpace())
                            {
                                var entrustUser = user.Get(entrustId);
                                if (null != entrustUser)
                                {
                                    userModel = entrustUser.Clone();
                                    userModel.Note = userModel.Id.ToString();//用这个字段来保存委托人ID,在加入任务表的时候要用到
                                }
                            }
                            userList[i] = userModel;
                        }
                        #endregion
                        nextSteps.Add((nextStepId, userList, completedDateTime));
                    }

                }
            }
            executeModel.Steps = nextSteps;
            #endregion

            #region 保存业务数据
            //判断抄送完成和终止任务不保存数据
            if (executeModel.ExecuteType != Model.FlowRunModel.Execute.Type.CopyforCompleted 
                && executeModel.ExecuteType != Model.FlowRunModel.Execute.Type.TaskEnd)
            {
                var (newInstanceId, errMsg) = new Business.Form().SaveData(Request);
                if (!errMsg.IsNullOrWhiteSpace())
                {
                    return new ContentResult() { Content = errMsg };
                }
                if (executeModel.InstanceId.IsNullOrWhiteSpace())
                {
                    executeModel.InstanceId = newInstanceId;
                }
            }
            #endregion

            #region 设置任务标题
            string taskTitle = string.Empty;
            string form_dbtabletitleexpression = Request.Forms("form_dbtabletitleexpression");//标题表达式
            if (form_dbtabletitleexpression.IsNullOrWhiteSpace())
            {
                string form_dbtabletitle = Request.Forms("form_dbtabletitle");
                taskTitle = Request.Forms(Request.Forms("form_dbtable") + "-" + form_dbtabletitle);
            }
            else
            {
                string tableName = Request.Forms("Form_DBTable");
                taskTitle = new Business.Form().ReplaceTitleExpression(form_dbtabletitleexpression, tableName, executeModel.InstanceId, Request);
            }
            executeModel.Title = taskTitle;
            #endregion

            var executeResult = flowTask.Execute(executeModel);

            #region 归档
            if (currentStepModel.Archives == 1
                && (executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.Submit
                || executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.FreeSubmit
                || executeModel.ExecuteType == Model.FlowRunModel.Execute.Type.Completed))
            {
                var dbs = flowRunModel.Databases;
                if (dbs.Count > 0)
                {
                    string comments = Request.Forms("form_commentlist_div_textarea");
                    var db = dbs.First();
                    string formDataJson = new Business.Form().GetFormData(db.ConnectionId.ToString(),
                        db.Table, db.PrimaryKey, executeModel.InstanceId, executeModel.StepId.ToString(), executeModel.FlowId.ToString(),
                        Request.Forms("form_dataformatjson"), out string fieldStatusJSON);
                    string executeUserId = string.Empty;
                    string executeUserName = string.Empty;
                    if (executeResult.CurrentTask != null)
                    {
                        executeUserId = executeResult.CurrentTask.ReceiveOrganizeId.HasValue
                            ? Business.Organize.PREFIX_RELATION + executeResult.CurrentTask.ReceiveOrganizeId.Value.ToString()
                            : Business.Organize.PREFIX_USER + executeResult.CurrentTask.ReceiveId.ToString();
                        executeUserName = executeResult.CurrentTask.ReceiveName;
                    }
                    else
                    {
                        executeUserId = Business.Organize.PREFIX_USER + executeModel.Sender.Id.ToString();
                        executeUserName = executeModel.Sender.Name;
                    }
                    Model.FlowArchive flowArchiveModel = new Model.FlowArchive
                    {
                        Comments = comments.Trim(),
                        DataJson = formDataJson,
                        FlowId = flowRunModel.Id,
                        FlowName = flowRunModel.Name,
                        Id = Guid.NewGuid(),
                        InstanceId = executeModel.InstanceId,
                        StepId = currentStepModel.Id,
                        StepName = currentStepModel.Name,
                        TaskId = executeResult.CurrentTask == null ? executeModel.TaskId : executeResult.CurrentTask.Id,
                        Title = executeResult.CurrentTask == null ? executeModel.Title : executeResult.CurrentTask.Title,
                        UserId = executeUserId,
                        UserName = executeUserName,
                        WriteTime = DateExtensions.Now
                    };
                    new Business.FlowArchive().Add(flowArchiveModel);
                }
            }
            #endregion

            Business.Log.Add("处理了流程[" + flowRunModel.Name + "] - 步骤[" + flowTask.GetStepName(flowRunModel, executeModel.StepId) + "] - 标题[" + executeModel.Title + "]", paramsJSON, Business.Log.Type.流程运行, oldContents: "执行参数：" + executeModel.ToString(), others: "返回：" + executeResult.ToString());
            string url = string.Empty;
            //如果下一步接收者中有当前人员则直接打开
            var nextTask = executeResult.NextTasks?.Find(p => p.ReceiveId == executeModel.Sender.Id && p.Status.In(0, 1));
            if (nextTask != null)
            {
                url = string.Format(Url.Content("~/RoadFlowCore/FlowRun/{0}?flowid={1}&stepid={2}&taskid={3}&groupid={4}&instanceid={5}&appid={6}&tabid={7}&rf_appopenmodel={8}"),
                "Index", nextTask.FlowId, nextTask.StepId, nextTask.Id, nextTask.GroupId, nextTask.InstanceId,
                Request.Querys("appid"), Request.Querys("tabid"), Request.Querys("rf_appopenmodel")
                );
            }
            ViewData["debutMsg"] = flowRunModel.Debug == 1 ? "执行参数：<br/>" + paramsJSON + "<br/><br/>参数实体：<br/>" + executeModel.ToString() + "<br/><br/>执行结果：<br/>" + executeResult.ToString() : "";
            ViewData["alertMsg"] = executeResult.Messages;
            ViewData["success"] = executeResult.IsSuccess ? "1" : "0";
            
            ViewData["url"] = url;
            return View();
        }

        /// <summary>
        /// 不通过流程，直接编辑表单
        /// </summary>
        /// <returns></returns>
        public IActionResult FormEdit()
        {
            string applibraryid = Request.Querys("applibraryid");
            string instanceid = Request.Querys("instanceid");
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string display = Request.Querys("display");
            string returnUrl = Request.Querys("returnurl");
            string showtoolbar = Request.Querys("showtoolbar");//是否显示工具栏，查看表单的时候，有时不需要显示。

            if (display.IsNullOrEmpty())
            {
                display = "0";
            }
            if (!applibraryid.IsGuid(out Guid applibraryId))
            {
                return new ContentResult() { Content = "应用程序库ID错误!" };
            }

            #region 得到表单地址
            bool isCustomeForm = false;//是否是自定义表单
            var appModel = new Business.AppLibrary().Get(applibraryId);
            string formUrl = string.Empty;
            if (null == appModel)
            {
                return new ContentResult() { Content = "未找到应用!" };
            }
            isCustomeForm = appModel.Code.IsNullOrWhiteSpace();
            if (isCustomeForm)
            {
                formUrl = appModel.Address;
            }
            else
            {
                formUrl = Url.Content("~/wwwroot/RoadFlowResources/scripts/formDesigner/form/" + appModel.Address);
            }
            #endregion

            string query = "applibraryid=" + applibraryid + "&instanceid=" + instanceid + "&appid=" + appid 
                + "&tabid=" + tabid + "&dilplay=" + display + "&iframeid=" + Request.Querys("iframeid") 
                + "&showtoolbar=" + showtoolbar + "&rf_appopenmodel=" + Request.Querys("rf_appopenmodel") 
                + "&isrefresparent=" + Request.Querys("isrefresparent") 
                + "&returnurl="+Request.Querys("returnurl").UrlEncode() + "&openerid=" + Request.Querys("openerid");

            #region 组织按钮
            List<Model.FlowButton> flowButtons = new List<Model.FlowButton>();
            if (!isCustomeForm)//如果是自定义表单这里不添加按钮，自己在表单中设置按钮
            {
                List<Model.FlowRunModel.StepButton> stepButtons = new List<Model.FlowRunModel.StepButton>();
                flowButtons.Add(new Model.FlowButton()
                {
                    Id = Guid.NewGuid(),
                    Title = "确认保存",
                    Ico = "fa-save",
                    Script = "saveEditFormData(a);",
                    Sort = 8
                });
                flowButtons.Add(new Model.FlowButton()
                {
                    Id = Guid.NewGuid(),
                    Title = "关闭窗口",
                    Ico = "fa-close",
                    Script = "closeWindw(" + Request.Querys("rf_appopenmodel") + ");",
                    Sort = 9
                });
                if (!returnUrl.IsNullOrWhiteSpace())
                {
                    flowButtons.Add(new Model.FlowButton()
                    {
                        Id = Guid.NewGuid(),
                        Title = "返回",
                        Ico = "fa-mail-reply",
                        Script = "window.location='" + returnUrl + "';",
                        Sort = 0
                    });
                }
            }
            #endregion

            ViewData["tabId"] = tabid;
            ViewData["appId"] = appid;
            ViewData["instanceId"] = instanceid;
            ViewData["display"] = display;
            ViewData["formUrl"] = formUrl;
            ViewData["query"] = query;
            ViewData["isCustomeForm"] = isCustomeForm ? "1" : "0";//是否是自定义表单
            ViewData["flowButtons"] = flowButtons.OrderBy(p => p.Sort).ToList();
            ViewData["request"] = Request;
            ViewData["title"] = appModel.Title;
            return View();
        }

        /// <summary>
        /// 保存不通过流程，直接编辑表单
        /// </summary>
        /// <returns></returns>
        public IActionResult FormEditSave()
        {
            var (newInstanceId, errMsg) = new Business.Form().SaveData(Request);
            if (errMsg.IsNullOrWhiteSpace() && !newInstanceId.IsNullOrWhiteSpace())
            {
                string url = "FormEdit?instanceid=" + newInstanceId + "&applibraryid=" + Request.Querys("applibraryid")
                    + "&appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid") + "&dilplay="
                    + Request.Querys("display") + "&isrefresparent=" + Request.Querys("isrefresparent") + "&showtoolbar=" + Request.Querys("showtoolbar") 
                    + "&iframeid=" + Request.Querys("iframeid") + "&openerid=" + Request.Querys("openerid")
                    + "&rf_appopenmodel=" + Request.Querys("rf_appopenmodel")+ "&returnurl=" + Request.Querys("returnurl").UrlEncode();
                //return new ContentResult() { Content = "<script>alert('保存成功!');parent.location='" + url + "';</script>", ContentType = "text/html" };
                ViewData["issuccess"] = "1";
                ViewData["msg"] = "保存成功!";
                ViewData["url"] = url;
            }
            else
            {
                ViewData["issuccess"] = "0";
                ViewData["msg"] = errMsg;
                ViewData["url"] = "";
                //return new ContentResult() { Content = "<script>alert('" + errMsg + "!')</script>", ContentType = "text/html" };
            }
            ViewData["rf_appopenmodel"] = Request.Querys("rf_appopenmodel");
            ViewData["isrefresparent"] = Request.Querys("isrefresparent");
            return View();
        }

    }
}