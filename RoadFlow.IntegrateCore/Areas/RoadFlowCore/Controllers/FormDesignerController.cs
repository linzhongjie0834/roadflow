﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FormDesignerController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            string queryString = Request.UrlQuery();
            ViewData["queryString"] = queryString.IsNullOrWhiteSpace() ? "?1=1" : Request.UrlQuery();
            return View();
        }

        [Validate]
        public IActionResult Tree()
        {
            ViewData["appId"] = Request.Querys("appid");
            ViewData["iframeId"] = Request.Querys("iframeid");
            ViewData["openerId"] = Request.Querys("openerid");
            ViewData["rootId"] = new Business.Dictionary().GetIdByCode("system_applibrarytype_form");
            return View();
        }

        [Validate]
        public IActionResult List()
        {
            ViewData["appId"] = Request.Querys("appid");
            ViewData["iframeId"] = Request.Querys("iframeid");
            ViewData["openerId"] = Request.Querys("openerid");
            ViewData["query"] = "typeid=" + Request.Querys("typeid") + "&appid=" + Request.Querys("appid")
                + "&iframeid=" + Request.Querys("iframeid") + "&openerid=" + Request.Querys("openerid");
            return View();
        }

        [Validate]
        public string QueryList()
        {
            string form_name = Request.Forms("form_name");
            string typeid = Request.Querys("typeid");
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");

            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            string order = (sidx.IsNullOrEmpty() ? "CreateDate" : sidx) + " " + (sord.IsNullOrEmpty() ? "DESC" : sord);
            if (typeid.IsGuid(out Guid typeId))
            {
                var childsId = new Business.Dictionary().GetAllChildsId(typeId);
                typeid = childsId.JoinSqlIn();
            }
            Business.Form form = new Business.Form();
            var forms = form.GetPagerList(out int count, size, number, form_name, typeid, order);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            Business.User user = new Business.User();
            foreach (System.Data.DataRow dr in forms.Rows)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Name", dr["Name"].ToString() },
                    { "CreateUserName", dr["CreateUserName"].ToString() },
                    { "CreateTime",  dr["CreateDate"].ToString().ToDateTime().ToDateTimeString() },
                    { "LastModifyTime", dr["EditDate"].ToString().ToDateTime().ToDateTimeString() },
                    { "Edit", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"openform('" + dr["Id"].ToString() + "', '" + dr["Name"].ToString() + "');return false;\"><i class=\"fa fa-edit (alias)\"></i>编辑</a>" +
                    "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"delform('" + dr["Id"].ToString() + "');return false;\"><i class=\"fa fa-remove\"></i>删除</a>" }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        [Validate]
        public IActionResult Index1()
        {
            string formid = Request.Querys("formid");
            string attr = "{}", subtable = "[]", events = "[]", html = string.Empty;
            if (formid.IsGuid(out Guid fid))
            {
                var formModel = new Business.Form().Get(fid);
                if (null != formModel)
                {
                    attr = formModel.attribute;
                    subtable = formModel.SubtableJSON;
                    events = formModel.EventJSON;
                    html = formModel.Html;
                }
            }
            ViewData["attr"] = attr;
            ViewData["subtable"] = subtable;
            ViewData["events"] = events;
            ViewData["html"] = html;
            ViewData["isNewForm"] = Request.Querys("isnewform");
            ViewData["typeId"] = Request.Querys("typeid");
            ViewData["query"] = "typeid=" + Request.Querys("typeid") + "&appid=" + Request.Querys("appid")
               + "&iframeid=" + Request.Querys("iframeid") + "&openerid=" + Request.Querys("openerid");
            ViewData["dbconnOptions"] = new Business.DbConnection().GetOptions();
            return View();
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Delete()
        {
            string formid = Request.Forms("formid");
            if (!formid.IsGuid(out Guid fid))
            {
                return "表单ID为空!";
            }
            Business.Form form = new Business.Form();
            var formModel = form.Get(fid);
            if (null == formModel)
            {
                return "没有找到要删除的表单!";
            }
            int i = new Business.Form().DeleteAndApplibrary(formModel);
            Business.Log.Add("删除了表单-" + formModel.Name, formModel.ToString(), Business.Log.Type.流程管理);
            return "删除成功!";
        }

        /// <summary>
        /// 得到下拉联动选项
        /// </summary>
        /// <returns></returns>
        public string GetChildOptions()
        {
            string source = Request.Forms("source");
            string value = Request.Forms("value");
            string connid = Request.Forms("connid");
            string text = Request.Forms("text");
            string dictvaluefield = Request.Forms("dictvaluefield");
            string dictid = Request.Forms("dictid");
            string dictIschild = Request.Forms("dictIschild");
            string defaultvalue = Request.Forms("defaultvalue");
            return new Business.Form().GetChildOptions(source, connid, text, value, dictvaluefield, dictid, defaultvalue, "1".Equals(dictIschild));
        }
    }
}