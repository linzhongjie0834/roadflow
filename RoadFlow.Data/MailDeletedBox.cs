﻿using RoadFlow.Mapper;
using RoadFlow.Utility;
using System;
using System.Data;
using System.Data.Common;

namespace RoadFlow.Data
{
  public class MailDeletedBox
  {
    public RoadFlow.Model.MailDeletedBox Get(Guid id)
    {
      using (DataContext dataContext = new DataContext())
        return dataContext.Find<RoadFlow.Model.MailDeletedBox>((object) id);
    }

    public int Add(RoadFlow.Model.MailDeletedBox mailDeletedBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Add<RoadFlow.Model.MailDeletedBox>(mailDeletedBox);
        return dataContext.SaveChanges();
      }
    }

    public int Update(RoadFlow.Model.MailDeletedBox mailDeletedBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Update<RoadFlow.Model.MailDeletedBox>(mailDeletedBox);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(RoadFlow.Model.MailDeletedBox mailDeletedBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Remove<RoadFlow.Model.MailDeletedBox>(mailDeletedBox);
        return dataContext.SaveChanges();
      }
    }

    public int Recovery(RoadFlow.Model.MailDeletedBox mailDeletedBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Add<RoadFlow.Model.MailInBox>(new RoadFlow.Model.MailInBox()
        {
          ContentsId = mailDeletedBox.ContentsId,
          Id = mailDeletedBox.Id,
          IsRead = mailDeletedBox.IsRead,
          OutBoxId = mailDeletedBox.OutBoxId,
          ReadDateTime = mailDeletedBox.ReadDateTime,
          SendDateTime = mailDeletedBox.SendDateTime,
          SendUserId = mailDeletedBox.SendUserId,
          Subject = mailDeletedBox.Subject,
          SubjectColor = mailDeletedBox.SubjectColor,
          UserId = mailDeletedBox.UserId
        });
        dataContext.Remove<RoadFlow.Model.MailDeletedBox>(mailDeletedBox);
        return dataContext.SaveChanges();
      }
    }

    public DataTable GetPagerList(out int count, int size, int number, Guid currentUserId, string subject, string userId, string date1, string date2, string order)
    {
      using (DataContext dataContext = new DataContext())
      {
        DbconnnectionSql dbconnnectionSql = new DbconnnectionSql(Config.DatabaseType);
        ValueTuple<string, DbParameter[]> mailDeletedBoxSql = dbconnnectionSql.SqlInstance.GetMailDeletedBoxSql(currentUserId, subject, userId, date1, date2);
        string sql = mailDeletedBoxSql.Item1;
        DbParameter[] parameters = mailDeletedBoxSql.Item2;
        string paerSql = dbconnnectionSql.SqlInstance.GetPaerSql(sql, size, number, out count, parameters, order);
        return dataContext.GetDataTable(paerSql, parameters);
      }
    }
  }
}
