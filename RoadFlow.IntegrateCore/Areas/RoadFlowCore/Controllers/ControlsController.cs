﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using RoadFlow.Utility;
using System.Xml.Linq;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class ControlsController : Controller
    {
        public ControlsController()
        {
            webRootPath = Current.WebRootPath;
            contentRootPath = Current.ContentRootPath;
            attachmentPath = contentRootPath + "/Attachment/";//上传附件路径
        }

        #region 选择组织架构
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public IActionResult Member_Index()
        {
            string values = Request.Querys("values");
            string eid = Request.Querys("eid");
            string isunit = Request.Querys("isunit");
            string isdept = Request.Querys("isdept");
            string isstation = Request.Querys("isstation");
            string isuser = Request.Querys("isuser");
            string ismore = Request.Querys("ismore");
            string isall = Request.Querys("isall");
            string isgroup = Request.Querys("isgroup");
            string isrole = Request.Querys("isrole");
            string rootid = Request.Querys("rootid");
            string ischangetype = Request.Querys("ischangetype");
            string isselect = Request.Querys("isselect");

            Business.Organize organize = new Business.Organize();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string value in values.Split(','))
            {
                if (value.IsNullOrEmpty())
                {
                    continue;
                }
                string name = organize.GetNames(value);
                if (name.IsNullOrEmpty())
                {
                    continue;
                }
                stringBuilder.AppendFormat("<div onclick=\"currentDel=this;showinfo('{0}');\" class=\"selectorDiv\" ondblclick=\"currentDel=this;del();\" value=\"{0}\">", value);
                stringBuilder.Append(name);
                stringBuilder.Append("</div>");
            }

            ViewData["eid"] = eid;
            ViewData["isunit"] = isunit;
            ViewData["isdept"] = isdept;
            ViewData["isstation"] = isstation;
            ViewData["isuser"] = isuser;
            ViewData["ismore"] = ismore;
            ViewData["isall"] = isall;
            ViewData["isgroup"] = isgroup;
            ViewData["isrole"] = isrole;
            ViewData["rootid"] = rootid;
            ViewData["ischangetype"] = ischangetype;
            ViewData["isselect"] = isselect;
            ViewData["values"] = values;
            ViewData["userprefix"] = Business.Organize.PREFIX_USER;
            ViewData["relationprefix"] = Business.Organize.PREFIX_RELATION;
            ViewData["workgroupprefix"] = Business.Organize.PREFIX_WORKGROUP;
            ViewData["defaultValues"] = stringBuilder.ToString();
            return View();
        }
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public string Member_GetNames()
        {
            return new Business.Organize().GetNames(Request.Forms("values"));
        }
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public string Member_GetNote()
        {
            string id = Request.Querys("id");
            if (id.IsNullOrWhiteSpace())
            {
                return "";
            }
            Business.Organize organize = new Business.Organize();
            Business.User user = new Business.User();
            Business.OrganizeUser organizeUser = new Business.OrganizeUser();
            if (id.StartsWith(Business.Organize.PREFIX_USER))//人员
            {
                var organizeUserModel = organizeUser.GetMainByUserId(id.RemoveUserPrefix().ToGuid());
                return organize.GetParentsName(organizeUserModel.OrganizeId) + " \\ " + organize.GetName(organizeUserModel.OrganizeId);
            }
            else if (id.StartsWith(Business.Organize.PREFIX_RELATION))//兼职人员
            {
                var organizeUserModel = organizeUser.Get(id.RemoveUserRelationPrefix().ToGuid());
                return organize.GetParentsName(organizeUserModel.OrganizeId) + " \\ " + organize.GetName(organizeUserModel.OrganizeId) + "[兼任]";
            }
            else if (id.StartsWith(Business.Organize.PREFIX_WORKGROUP))//工作组
            {
                return "";
            }
            else if (id.IsGuid(out Guid gid))
            {
                return organize.GetParentsName(gid) + " \\ " + organize.GetName(gid);
            }
            return "";
        }
        #endregion

        #region 选择图标
        private readonly string webRootPath;
        private readonly string contentRootPath;
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public IActionResult SelectIco_Index()
        {
            ViewData["source"] = Request.Querys("source");
            ViewData["id"] = Request.Querys("id");
            return View();
        }
        public string SelectIco_File()
        {
            XElement rootElement = new XElement("Root");
            string Path = Request.Querys("path");
            if (Path.IsNullOrWhiteSpace())
            {
                Path = "/RoadFlowResources/images/ico";
            }

            string showType = ",.jpg,.gif,.png,";
            //string webRootPath = _hostingEnvironment.WebRootPath;
            //string contentRootPath = _hostingEnvironment.ContentRootPath;

            if (!Directory.Exists(webRootPath + Path))
            {
                return rootElement.ToString();
            }

            DirectoryInfo folder = new DirectoryInfo(webRootPath + Path);
            XElement element;
            foreach (var item in folder.GetFiles().Where(p => (p.Attributes & FileAttributes.Hidden) == 0))
            {
                if (showType.IndexOf("," + item.Extension.ToLower() + ",") != -1)
                {
                    element = new XElement("Icon");
                    rootElement.Add(element);
                    element.SetAttributeValue("title", item.Name);
                    element.SetAttributeValue("path", "/RoadFlowResources/images/ico/" + item.Name);
                    element.SetAttributeValue("path1", "/RoadFlowResources/images/ico/" + item.Name);
                }
            }
            return rootElement.ToString();
        }
        #endregion

        #region 选择数据字典
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public IActionResult Dictionary_Index()
        {
            string values = Request.Querys("values");
            string dataSource = Request.Querys("datasource");
            StringBuilder stringBuilder = new StringBuilder();
            Business.Dictionary dictionary = new Business.Dictionary();
            foreach (string value in values.Split(","))
            {
                switch (dataSource)
                {
                    case "0":
                        if (value.IsGuid(out Guid dictId))
                        {
                            var dictModel = dictionary.Get(dictId);
                            if (null != dictModel)
                            {
                                stringBuilder.Append("<div onclick=\"currentDel=this;showinfo('{0}');\" class=\"selectorDiv\" ondblclick=\"currentDel=this;del();\" value=\"" + value + "\">");
                                stringBuilder.Append(dictModel.Title);
                                stringBuilder.Append("</div>");
                            }
                        }
                        break;
                }
            }
            ViewData["defaults"] = stringBuilder.ToString();
            ViewData["ismore"] = Request.Querys("ismore");
            ViewData["isparent"] = Request.Querys("isparent");
            ViewData["ischild"] = Request.Querys("ischild");
            ViewData["isroot"] = Request.Querys("isroot");
            ViewData["root"] = Request.Querys("root");
            ViewData["eid"] = Request.Querys("eid");
            ViewData["datasource"] = dataSource;
            return View();
        }
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public string Dictionary_GetNames()
        {
            string values = Request.Forms("values");
            StringBuilder stringBuilder = new StringBuilder();
            Business.Dictionary dictionary = new Business.Dictionary();
            foreach (string value in values.Split(","))
            {
                if (value.IsGuid(out Guid dictId))
                {
                    var dictModel = dictionary.Get(dictId);
                    if (null != dictModel)
                    {
                        stringBuilder.Append(dictModel.Title);
                        stringBuilder.Append("、");
                    }
                }
            }
            return stringBuilder.ToString().TrimEnd('、');
        }
        #endregion

        #region 附件上传
        private readonly string attachmentPath;
        [Validate(CheckLogin = true, CheckApp = false, CheckUrl = false)]
        public IActionResult UploadFiles_Index()
        {
            JArray jArray = new JArray();
            string values = Request.Querys("value");
            foreach (string value in (values ?? "").Split('|'))
            {
                string fileName = value.DESDecrypt();
                FileInfo fileInfo = new FileInfo(attachmentPath + fileName);
                if (!fileInfo.Exists)
                {
                    continue;
                }
                JObject jObject = new JObject
                {
                    { "id", value },
                    { "name", fileInfo.Name },
                    { "size", fileInfo.Length.ToFileSize() }
                };
                jArray.Add(jObject);
            }
            ViewData["fileType"] = Request.Querys("filetype");
            ViewData["eid"] = Request.Querys("eid");
            ViewData["userId"] = Current.UserId;
            ViewData["values"] = jArray.ToString();
            return View();
        }
        [Validate(CheckApp = false, CheckUrl = false)]
        [ValidateAntiForgeryToken]
        public string UploadFiles_Save()
        {
            string month = DateExtensions.Now.ToString("yyyyMMdd");
            var files = Request.Form.Files;
            string filetype = Request.Forms("filetype");
            JObject jObject = new JObject();
            if (files.Count > 0)
            {
                var file = files.First();
                string extName = Path.GetExtension(file.FileName).TrimStart('.');
                if (",exe,msi,".ContainsIgnoreCase("," + extName + ","))
                {
                    jObject.Add("error", "不能上传该类型文件");
                    return jObject.ToString();
                }
                if (!filetype.IsNullOrWhiteSpace() && !("," + filetype + ",").ContainsIgnoreCase("," + extName + ","))
                {
                    jObject.Add("error", "不能上传该类型文件");
                    return jObject.ToString();
                }
                string saveDir = attachmentPath + month + "/";
                string fileName = file.FileName.Replace(" ", "");
                string newFileName = GetUploadFileName(saveDir, fileName);
                if (!Directory.Exists(saveDir))
                {
                    Directory.CreateDirectory(saveDir);
                }
                using (FileStream fs = System.IO.File.Create(saveDir + newFileName))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                jObject.Add("id", (month + "/" + newFileName).DESEncrypt());
                jObject.Add("size", file.Length.ToFileSize());
            }
            return jObject.ToString();
        }
        /// <summary>
        /// 得到上传文件名，如果重名要重新命名
        /// </summary>
        /// <param name="saveDir"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetUploadFileName(string saveDir, string fileName)
        {
            if (System.IO.File.Exists(saveDir + fileName))
            {
                string extName = Path.GetExtension(fileName);
                string fName = Path.GetFileNameWithoutExtension(fileName) + "_" + Tools.GetRandomString(6).ToUpper();
                return GetUploadFileName(saveDir, fName + extName);
            }
            return fileName;
        }
        private string GetHeadType(String extName)
        {
            if (",jpg,jpeg,png,gif,tif,tiff,txt,json,xml,pdf,doc,docx,xls,xlsx,ppt,pptx".ContainsIgnoreCase("," + extName + ","))
            {
                return "inline";
            }
            else
            {
                return "attachment";
            }
        }
        /// <summary>
        /// 显示文件
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false, CheckUrl = false)]
        public void ShowFile()
        {
            string file = Request.Querys("file").DESDecrypt();
            FileInfo tmpFile = new FileInfo(attachmentPath + file);

            if (!tmpFile.Exists)
            {
                return;
            }
            string fileName = tmpFile.Name.UrlEncode();
            //if (Request != null && (Request..StartsWith("IE", StringComparison.CurrentCultureIgnoreCase)
            //    || context.Request.Browser.Type.StartsWith("InternetExplorer", StringComparison.CurrentCultureIgnoreCase)))
            //{
            //    fileName = fileName.UrlEncode();
            //}

            Response.Headers.Add("Server-FileName", fileName);
            string tmpContentType = GetHeadType(Path.GetExtension(file).TrimStart('.'));
            if (string.IsNullOrEmpty(tmpContentType))
            {
                Response.ContentType = "application/octet-stream";
                Response.Headers.Add("Content-Disposition", "attachment; filename=" + fileName);
            }
            else
            {
                Response.ContentType = tmpContentType;
                Response.Headers.Add("Content-Disposition", "inline; filename=" + fileName);
            }
            Response.Headers.Add("Content-Length", tmpFile.Length.ToString());
            using (var tmpRead = tmpFile.OpenRead())
            {
                var tmpByte = new byte[2048];
                var i = tmpRead.Read(tmpByte, 0, tmpByte.Length);
                while (i > 0)
                {
                    Response.Body.Write(tmpByte, 0, i);
                    Response.Body.Flush();
                    i = tmpRead.Read(tmpByte, 0, tmpByte.Length);
                }
            }
            Response.Body.Flush();
            Response.Body.Close();
        }
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false, CheckUrl = false)]
        [ValidateAntiForgeryToken]
        public string DeleteFile()
        {
            string file = Request.Forms("file").ToString().DESDecrypt();
            if (file.IsNullOrWhiteSpace())
            {
                return "文件为空!";
            }
            FileInfo fileInfo = new FileInfo(attachmentPath + file);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
            return "1";
        }
        /// <summary>
        /// 保存编辑器上传的文件
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false, CheckUrl = false)]
        public string SaveCKEditorFiles()
        {
            string month = DateExtensions.Now.ToString("yyyyMMdd");
            var files = Request.Form.Files;
            JObject jObject = new JObject();
            if (files.Count == 0)
            {
                jObject.Add("number", -1);
                jObject.Add("message", "没有要上传的文件");
                return new JObject() { { "error", jObject } }.ToString();
            }
            var file = files.First();
            string extName = Path.GetExtension(file.FileName).TrimStart('.');
            if (",exe,msi,".ContainsIgnoreCase("," + extName + ","))
            {
                jObject.Add("number", -1);
                jObject.Add("message", "不能上传该类型文件");
                return new JObject() { { "error", jObject } }.ToString();
            }
            string saveDir = attachmentPath + month + "/";
            string fileName = file.FileName.Replace(" ", "");
            string newFileName = GetUploadFileName(saveDir, fileName);
            if (!Directory.Exists(saveDir))
            {
                Directory.CreateDirectory(saveDir);
            }
            using (FileStream fs = System.IO.File.Create(saveDir + newFileName))
            {
                file.CopyTo(fs);
                fs.Flush();
            }
            JObject jObject1 = new JObject
            {
                { "fileName", newFileName },
                { "uploaded", 1 },
                { "url", Url.Content("~/RoadFlowCore/Controls/ShowFile?file=") + (month + "/" + newFileName).DESEncrypt() }
            };
            return jObject1.ToString();
        }
        /// <summary>
        /// 得到附件显示字符串
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false, CheckUrl = false)]
        public string UploadFiles_GetShowString()
        {
            string files = Request.Forms("files");
            return files.ToFilesShowString(false);
        }
        #endregion
    }
}