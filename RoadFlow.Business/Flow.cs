﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using RoadFlow.Utility;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.IO;

namespace RoadFlow.Business
{
    public class Flow
    {
        private readonly Data.Flow flowData;

        public Flow()
        {
            flowData = new Data.Flow();
        }
        /// <summary>
        /// 得到所有流程
        /// </summary>
        /// <returns></returns>
        public List<Model.Flow> GetAll()
        {
            return flowData.GetAll();
        }
        /// <summary>
        /// 查询一个流程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Flow Get(Guid id)
        {
            return flowData.Get(id);
        }
        /// <summary>
        /// 添加一个流程
        /// </summary>
        /// <param name="flow">流程实体</param>
        /// <returns></returns>
        public int Add(Model.Flow flow)
        {
            return flowData.Add(flow);
        }
        /// <summary>
        /// 更新流程
        /// </summary>
        /// <param name="flow">流程实体</param>
        public int Update(Model.Flow flow)
        {
            return flowData.Update(flow);
        }
        /// <summary>
        /// 删除流程
        /// </summary>
        /// <param name="flow">流程实体</param>
        /// <returns></returns>
        public int Delete(Model.Flow flow)
        {
            return flowData.Delete(flow);
        }
        /// <summary>
        /// 得到所有流程下拉选项
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetOptions(string value = "")
        {
            return GetFlowOptions(GetAll(), value);
        }

        /// <summary>
        /// 得到可管理实例的流程下拉选项
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetManageInstanceOptions(Guid userId, string value = "")
        {
            return GetFlowOptions(GetManageInstanceFlow(userId), value);
        }

        private string GetFlowOptions(List<Model.Flow> flows, string value = "")
        {
            StringBuilder stringBuilder = new StringBuilder();
            Dictionary dictionary = new Dictionary();
            Guid dictRootID = dictionary.GetIdByCode("system_applibrarytype_flow");
            foreach (var flow in flows)
            {
                stringBuilder.Append("<option value=\"" + flow.Id + "\"");
                if (flow.Id.ToString().EqualsIgnoreCase(value))
                {
                    stringBuilder.Append(" selected=\"selected\"");
                }
                stringBuilder.Append(">" + flow.Name + " (" + dictionary.GetAllParentTitle(flow.FlowType, true, false, dictRootID.ToString()) + ")");
                stringBuilder.Append("</option>");
            }
            return stringBuilder.ToString();
        }


        /// <summary>
        /// 查询一页数据
        /// </summary>
        /// <param name="count"></param>
        /// <param name="size"></param>
        /// <param name="number"></param>
        /// <param name="flowIdList"></param>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="order"></param>
        /// <param name="status">状态-1表示查询未删除的流程</param>
        /// <returns></returns>
        public System.Data.DataTable GetPagerList(out int count, int size, int number, List<Guid> flowIdList, string name, string type, string order, int status = -1)
        {
            return flowData.GetPagerList(out count, size, number, flowIdList, name, type, order, status);
        }

        /// <summary>
        /// 得到状态显示
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public string GetStatusTitle(int status)
        {
            string title = string.Empty;
            switch (status)
            {
                case 0:
                    title = "设计中";
                    break;
                case 1:
                    title = "已安装";
                    break;
                case 2:
                    title = "已卸载";
                    break;
                case 3:
                    title = "已删除";
                    break;
            }
            return title;
        }

        /// <summary>
        /// 保存流程
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public string Save(string json)
        {
            JObject jObject = null;
            try
            {
                jObject = JObject.Parse(json);
            }
            catch
            {
                return "JSON解析错误";
            }
            if (null == jObject)
            {
                return "JSON解析错误";
            }
            string flowId = jObject.Value<string>("id");
            string name = jObject.Value<string>("name");
            string type = jObject.Value<string>("type");
            if (!flowId.IsGuid(out Guid fid))
            {
                return "流程ID错误";
            }
            if (name.IsNullOrWhiteSpace())
            {
                return "流程名称为空";
            }
            if (!type.IsGuid(out Guid typeId))
            {
                return "流程分类错误";
            }
            Flow flow = new Flow();
            Model.Flow flowModel = flow.Get(fid);
            bool isAdd = false;
            if (null == flowModel)
            {
                isAdd = true;
                flowModel = new Model.Flow
                {
                    Id = fid,
                    CreateDate = DateExtensions.Now,
                    CreateUser = User.CurrentUserId,
                    Status = 0
                };
            }
            flowModel.DesignerJSON = json;
            flowModel.InstanceManager = jObject.Value<string>("instanceManager");
            flowModel.Manager = jObject.Value<string>("manager");
            flowModel.Name = name;
            flowModel.FlowType = typeId;
            flowModel.Note = jObject.GetValue("note").ToString();
            if (isAdd)
            {
                flow.Add(flowModel);
            }
            else
            {
                flow.Update(flowModel);
            }
            Log.Add("保存了流程-" + flowModel.Name, json, Log.Type.流程管理);
            return "1";
        }

        /// <summary>
        /// 安装流程
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public string Install(string json)
        {
            string saveMsg = Save(json);
            if (!"1".Equals(saveMsg))
            {
                return saveMsg;
            }
            var flowRunModel = GetFlowRunModel(json, out string errMsg);
            if (null == flowRunModel)
            {
                return errMsg;
            }
            var flowModel = Get(flowRunModel.Id);
            if (null == flowModel)
            {
                return "未找到流程实体";
            }
            flowModel.InstallDate = DateExtensions.Now;
            flowModel.InstallUser = User.CurrentUserId;
            flowModel.RunJSON = json;
            flowModel.Status = 1;
            flowData.Install(flowModel);
            //清除运行时缓存
            ClearCache(flowModel.Id);
            Log.Add("安装了流程-" + flowModel.Name, json, Log.Type.流程管理, others: errMsg);
            return "1";
        }

        private const string CACHEKEY = "roadflow_cache_flowrun_";

        /// <summary>
        /// 得到流程运行时实体
        /// </summary>
        /// <param name="id">流程ID</param>
        /// <param name="isCache">是否从缓存中取</param>
        /// <returns></returns>
        public Model.FlowRun GetFlowRunModel(Guid id, bool isCache = true)
        {
            string cacheKey = CACHEKEY + id.ToString("N");
            if (isCache)
            {
                object obj = Cache.IO.Get(cacheKey);
                if (null != obj)
                {
                    return (Model.FlowRun)obj;
                }
            }
            var flowModel = Get(id);
            if (null == flowModel)
            {
                return null;
            }
            var flowRunModel = GetFlowRunModel(flowModel.RunJSON.IsNullOrWhiteSpace() ? flowModel.DesignerJSON : flowModel.RunJSON, out string msg);
            if (null != flowRunModel)
            {
                Cache.IO.Insert(cacheKey, flowRunModel);
            }
            return flowRunModel;
        }

        /// <summary>
        /// 清除流程运行时缓存
        /// </summary>
        /// <param name="id"></param>
        public void ClearCache(Guid id)
        {
            Cache.IO.Remove(CACHEKEY + id.ToString("N"));
        }

        /// <summary>
        /// 得到流程运行时实体
        /// </summary>
        /// <param name="json">流程设置JSON</param>
        /// <param name="errMsg">加载错误时的错误信息</param>
        /// <returns></returns>

        private Model.FlowRun GetFlowRunModel(string json, out string errMsg)
        {
            errMsg = string.Empty;
            JObject jObject = null;
            try
            {
                jObject = JObject.Parse(json);
            }
            catch
            {
                errMsg = "JSON解析错误";
                return null;
            }
            if (null == jObject)
            {
                errMsg = "JSON解析错误";
                return null;
            }
            string id = jObject.Value<string>("id");
            string name = jObject.Value<string>("name");
            string type = jObject.Value<string>("type");
            string mananger = jObject.Value<string>("manager");
            string instanceManager = jObject.Value<string>("instanceManager");
            if (!id.IsGuid(out Guid flowId))
            {
                errMsg = "流程ID错误";
                return null;
            }
            if (name.IsNullOrWhiteSpace())
            {
                errMsg = "流程名称为空";
                return null;
            }
            if (!type.IsGuid(out Guid typeId))
            {
                errMsg = "流程分类错误";
                return null;
            }
            if (mananger.IsNullOrWhiteSpace())
            {
                errMsg = "流程管理者为空";
                return null;
            }
            if (instanceManager.IsNullOrWhiteSpace())
            {
                errMsg = "流程实例管理者为空";
                return null;
            }
            var flowModel = Get(flowId);
            if (null == flowModel)
            {
                errMsg = "未找到该流程";
                return null;
            }
            var flowRunModel = new Model.FlowRun
            {
                Id = flowId,
                Name = name,
                Type = typeId,
                Manager = mananger,
                InstanceManager = instanceManager,
                FirstStepId = Guid.Empty,
                Note = jObject.Value<string>("note"),
                Debug = jObject.Value<string>("debug").IsInt(out int debug) ? debug : 0,
                DebugUserIds = new Organize().GetAllUsersId(jObject.Value<string>("debugUsers")),
                Status = flowModel.Status,
                CreateDate = flowModel.CreateDate,
                CreateUserId = flowModel.CreateUser,
                InstallDate = flowModel.InstallDate,
                InstallUserId = flowModel.InstallUser,
                Ico = jObject.Value<string>("ico"),
                Color = jObject.Value<string>("color")
            };

            #region 流程数据库连接信息
            JArray dbsArray = jObject.Value<JArray>("databases");
            List<Model.FlowRunModel.Database> databases = new List<Model.FlowRunModel.Database>();
            if (null != dbsArray)
            {
                foreach (JObject dbs in dbsArray)
                {
                    Model.FlowRunModel.Database database = new Model.FlowRunModel.Database
                    {
                        ConnectionId = dbs.Value<string>("link").IsGuid(out Guid cid) ? cid : Guid.Empty,
                        ConnectionName = dbs.Value<string>("linkName"),
                        Table = dbs.Value<string>("table"),
                        PrimaryKey = dbs.Value<string>("primaryKey")
                    };
                    databases.Add(database);
                }
            }
            flowRunModel.Databases = databases;
            #endregion

            #region 流程标识字段信息
            JObject titleFieldObject = jObject.Value<JObject>("titleField");
            Model.FlowRunModel.TitleField titleField = new Model.FlowRunModel.TitleField();
            if (null != titleFieldObject)
            {
                titleField.ConnectionId = titleFieldObject.Value<string>("link").IsGuid(out Guid cid) ? cid : Guid.Empty;
                titleField.Table = titleFieldObject.Value<string>("table");
                titleField.Field = titleFieldObject.Value<string>("field");
                titleField.Value = titleFieldObject.Value<string>("value");
            }
            flowRunModel.TitleField = titleField;
            #endregion

            #region 步骤基本信息
            JArray stepArray = jObject.Value<JArray>("steps");
            List<Model.FlowRunModel.Step> steps = new List<Model.FlowRunModel.Step>();
            if (null != stepArray)
            {
                foreach (JObject stepObject in stepArray)
                {
                    Model.FlowRunModel.Step stepModel = new Model.FlowRunModel.Step();
                    #region 坐标信息
                    JObject positionObject = stepObject.Value<JObject>("position");
                    if (null != positionObject)
                    {
                        stepModel.Position_X = positionObject.Value<string>("x").IsDecimal(out decimal x) ? x : 0;
                        stepModel.Position_Y = positionObject.Value<string>("y").IsDecimal(out decimal y) ? y : 0;
                    }
                    #endregion

                    #region 步骤信息
                    stepModel.Archives = stepObject.Value<string>("archives").IsInt(out int archives) ? archives : 0;
                    stepModel.ExpiredPrompt = stepObject.Value<string>("expiredPrompt").IsInt(out int expiredPrompt) ? expiredPrompt : 0;
                    stepModel.Id = stepObject.Value<string>("id").IsGuid(out Guid sid) ? sid : Guid.Empty;
                    stepModel.Type = stepObject.Value<string>("type").EqualsIgnoreCase("normal") ? 0 : 1;
                    stepModel.Name = stepObject.Value<string>("name");
                    stepModel.Note = stepObject.Value<string>("note");
                    stepModel.CommentDisplay = stepObject.Value<string>("opinionDisplay").IsInt(out int opinionDisplay) ? opinionDisplay : 0;
                    stepModel.SignatureType = stepObject.Value<string>("signatureType").IsInt(out int signatureType) ? signatureType : 0;
                    stepModel.WorkTime = stepObject.Value<string>("workTime").IsDecimal(out decimal workTime) ? workTime : 0;
                    stepModel.SendShowMessage = stepObject.Value<string>("sendShowMsg");
                    stepModel.BackShowMessage = stepObject.Value<string>("backShowMsg");
                    stepModel.SendSetWorkTime = stepObject.Value<string>("sendSetWorkTime").IsInt(out int sendSetWorkTime) ? sendSetWorkTime : 0;
                    stepModel.ExpiredExecuteModel = stepObject.Value<string>("timeOutModel").IsInt(out int timeOutModel) ? timeOutModel : 0;

                    #region 基本信息
                    JObject baseObject = stepObject.Value<JObject>("behavior");
                    Model.FlowRunModel.StepBase stepBaseModel = new Model.FlowRunModel.StepBase();
                    if (null != baseObject)
                    {
                        stepBaseModel.BackModel = baseObject.Value<string>("backModel").IsInt(out int backModel) ? backModel : 0;
                        if (baseObject.Value<string>("backStep").IsGuid(out Guid backStepId))
                        {
                            stepBaseModel.BackStepId = backStepId;
                        }
                        stepBaseModel.BackType = baseObject.Value<string>("backType").IsInt(out int backType) ? backType : 0;
                        stepBaseModel.DefaultHandler = baseObject.Value<string>("defaultHandler");
                        stepBaseModel.FlowType = baseObject.Value<string>("flowType").IsInt(out int flowType) ? flowType : 0;
                        if (baseObject.Value<string>("handlerStep").IsGuid(out Guid handlerStepId))
                        {
                            stepBaseModel.HandlerStepId = handlerStepId;
                        }
                        stepBaseModel.HandlerType = baseObject.Value<string>("handlerType");
                        stepBaseModel.HanlderModel = baseObject.Value<string>("hanlderModel").IsInt(out int hanlderModel) ? hanlderModel : 0;
                        stepBaseModel.Percentage = baseObject.Value<string>("percentage").IsDecimal(out decimal percentage) ? percentage : 0;
                        stepBaseModel.RunSelect = baseObject.Value<string>("runSelect").IsInt(out int runSelect) ? runSelect : 0;
                        stepBaseModel.SelectRange = baseObject.Value<string>("selectRange");
                        stepBaseModel.ValueField = baseObject.Value<string>("valueField");
                        stepBaseModel.Countersignature = baseObject.Value<string>("countersignature").IsInt(out int countersignature) ? countersignature : 0;
                        stepBaseModel.CountersignaturePercentage = baseObject.Value<string>("countersignaturePercentage").IsDecimal(out decimal countersignaturePercentage) ? countersignaturePercentage : 0;
                        stepBaseModel.SubFlowStrategy = baseObject.Value<string>("subflowstrategy").IsInt(out int subflowstrategy) ? subflowstrategy : 0;
                        stepBaseModel.ConcurrentModel = baseObject.Value<string>("concurrentModel").IsInt(out int concurrentModel) ? concurrentModel : 0;
                        stepBaseModel.DefaultHandlerSqlOrMethod = baseObject.Value<string>("defaultHandlerSqlOrMethod");
                        stepBaseModel.AutoConfirm = baseObject.Value<string>("autoConfirm").ToInt(0);
                        stepBaseModel.SkipIdenticalUser = baseObject.Value<string>("skipIdenticalUser").ToInt(0);
                        stepBaseModel.SkipMethod = baseObject.Value<string>("skipMethod");
                        stepBaseModel.SendToBackStep = baseObject.Value<string>("sendToBackStep").ToInt(0);
                    }
                    stepModel.StepBase = stepBaseModel;
                    #endregion

                    #region 抄送
                    Model.FlowRunModel.StepCopyFor stepCopyForModel = new Model.FlowRunModel.StepCopyFor();
                    JObject copyForObject = stepObject.Value<JObject>("copyFor");
                    if (null != copyForObject)
                    {
                        stepCopyForModel.MemberId = copyForObject.Value<string>("memberId");
                        stepCopyForModel.HandlerType = copyForObject.Value<string>("handlerType");
                        stepCopyForModel.Steps = copyForObject.Value<string>("steps");
                        stepCopyForModel.MethodOrSql = copyForObject.Value<string>("methodOrSql");
                    }
                    stepModel.StepCopyFor = stepCopyForModel;
                    #endregion

                    #region 按钮信息
                    List<Model.FlowRunModel.StepButton> stepButtons = new List<Model.FlowRunModel.StepButton>();
                    JArray buttonArray = stepObject.Value<JArray>("buttons");
                    if (null != buttonArray)
                    {
                        foreach (JObject buttonObject in buttonArray)
                        {
                            Model.FlowRunModel.StepButton stepButtonModel = new Model.FlowRunModel.StepButton();
                            if (buttonObject.Value<string>("id").IsGuid(out Guid bid))
                            {
                                var flowButtonModel = new FlowButton().Get(bid);
                                stepButtonModel.Id = bid;
                                stepButtonModel.Note = "";
                                string showTitle = buttonObject.Value<string>("showTitle");
                                stepButtonModel.ShowTitle = showTitle;
                                stepButtonModel.Sort = buttonObject.Value<int>("sort");
                                if (null != flowButtonModel)
                                {
                                    stepButtonModel.Note = flowButtonModel.Note;
                                    stepButtonModel.ShowTitle = showTitle.IsNullOrWhiteSpace() ? flowButtonModel.Title : showTitle;
                                }
                            }
                            stepButtons.Add(stepButtonModel);
                        }
                    }
                    stepModel.StepButtons = stepButtons;
                    #endregion

                    #region 事件
                    JObject eventObject = stepObject.Value<JObject>("event");
                    Model.FlowRunModel.StepEvent stepEventModel = new Model.FlowRunModel.StepEvent();
                    if (null != eventObject)
                    {
                        stepEventModel.BackAfter = eventObject.Value<string>("backAfter");
                        stepEventModel.BackBefore = eventObject.Value<string>("backBefore");
                        stepEventModel.SubmitAfter = eventObject.Value<string>("submitAfter");
                        stepEventModel.SubmitBefore = eventObject.Value<string>("submitBefore");
                        stepEventModel.SubFlowActivationBefore = eventObject.Value<string>("subflowActivationBefore");
                        stepEventModel.SubFlowCompletedBefore = eventObject.Value<string>("subflowCompletedBefore");
                    }
                    stepModel.StepEvent = stepEventModel;
                    #endregion

                    #region 表单
                    JArray formArray = stepObject.Value<JArray>("forms");
                    Model.FlowRunModel.StepForm stepFormModel = new Model.FlowRunModel.StepForm();
                    if (null != formArray && formArray.Count > 0)
                    {
                        JObject formObject = (JObject)formArray.First;
                        if (formObject.Value<string>("id").IsGuid(out Guid formId))
                        {
                            stepFormModel.Id = formId;
                        }
                        stepFormModel.Name = formObject.Value<string>("name");
                        if (formObject.Value<string>("idApp").IsGuid(out Guid appId))
                        {
                            stepFormModel.MobileId = appId;
                        }
                        stepFormModel.MobileName = formObject.Value<string>("nameApp");
                    }
                    stepModel.StepForm = stepFormModel;
                    #endregion

                    #region 字段状态
                    JArray fieldArray = stepObject.Value<JArray>("fieldStatus");
                    List<Model.FlowRunModel.StepFieldStatus> stepFieldStatuses = new List<Model.FlowRunModel.StepFieldStatus>();
                    if (null != fieldArray)
                    {
                        foreach (JObject fieldObject in fieldArray)
                        {
                            Model.FlowRunModel.StepFieldStatus stepFieldStatusModel = new Model.FlowRunModel.StepFieldStatus
                            {
                                Check = fieldObject.Value<string>("check").IsInt(out int check) ? check : 0,
                                Field = fieldObject.Value<string>("field"),
                                Status = fieldObject.Value<string>("status").IsInt(out int status) ? status : 0
                            };
                            stepFieldStatuses.Add(stepFieldStatusModel);
                        }
                    }
                    stepModel.StepFieldStatuses = stepFieldStatuses;
                    #endregion

                    #region 子流程
                    JObject subflowObject = stepObject.Value<JObject>("subflow");
                    Model.FlowRunModel.StepSubFlow stepSubFlowModel = new Model.FlowRunModel.StepSubFlow();
                    if (null != subflowObject)
                    {
                        if (subflowObject.Value<string>("flowId").IsGuid(out Guid subId))
                        {
                            stepSubFlowModel.SubFlowId = subId;
                        }
                        stepSubFlowModel.SubFlowStrategy = subflowObject.Value<string>("flowStrategy").IsInt(out int flowStrategy) ? flowStrategy : 0;
                        stepSubFlowModel.TaskType = subflowObject.Value<string>("taskType").IsInt(out int taskType) ? taskType : 0;
                    }
                    stepModel.StepSubFlow = stepSubFlowModel;
                    #endregion

                    steps.Add(stepModel);
                    #endregion
                }
            }
            flowRunModel.Steps = steps;
            if (steps.Count == 0)
            {
                errMsg = "流程至少需要一个步骤";
                return null;
            }
            #endregion

            #region 连线信息
            JArray lineArray = jObject.Value<JArray>("lines");
            List<Model.FlowRunModel.Line> lines = new List<Model.FlowRunModel.Line>();
            if (null != lineArray)
            {
                foreach (JObject lineObject in lineArray)
                {
                    Model.FlowRunModel.Line lineModel = new Model.FlowRunModel.Line
                    {
                        Id = lineObject.Value<string>("id").IsGuid(out Guid lid) ? lid : Guid.Empty,
                        FromId = lineObject.Value<string>("from").IsGuid(out Guid fid) ? fid : Guid.Empty,
                        ToId = lineObject.Value<string>("to").IsGuid(out Guid tid) ? tid : Guid.Empty,
                        CustomMethod = lineObject.Value<string>("customMethod"),
                        SqlWhere = lineObject.Value<string>("sql")
                    };
                    if (lineObject.Value<JArray>("organize") != null)
                    {
                        lineModel.OrganizeExpression = lineObject.Value<JArray>("organize").ToString(Newtonsoft.Json.Formatting.None);
                    }
                    lines.Add(lineModel);
                }
            }
            flowRunModel.Lines = lines;
            #endregion

            #region 设置开始步骤
            Model.FlowRunModel.Step firstStep = null;
            foreach (var step in flowRunModel.Steps)
            {
                if (flowRunModel.Lines.Find(p => p.ToId == step.Id) == null)
                {
                    firstStep = step;
                    break;
                }
            }
            if (null == firstStep)
            {
                errMsg = "流程没有开始步骤";
                return null;
            }
            flowRunModel.FirstStepId = firstStep.Id;
            #endregion

            return flowRunModel;
        }

        /// <summary>
        /// 得到一个流程步骤的下一步骤集合
        /// </summary>
        /// <param name="flowRunModel">流程运行时实体</param>
        /// <param name="stepId">步骤ID</param>
        /// <returns></returns>
        public List<Model.FlowRunModel.Step> GetNextSteps(Model.FlowRun flowRunModel, Guid stepId)
        {
            List<Model.FlowRunModel.Step> steps = new List<Model.FlowRunModel.Step>();
            if (null == flowRunModel)
            {
                return steps;
            }
            var lines = flowRunModel.Lines.FindAll(p => p.FromId == stepId);
            foreach (var line in lines)
            {
                var step = flowRunModel.Steps.Find(p => p.Id == line.ToId);
                if (null != step)
                {
                    steps.Add(step);
                }
            }
            return steps.OrderBy(p => p.Position_Y).ThenBy(p => p.Position_X).ToList();
        }

        /// <summary>
        /// 得到一个步骤的前面步骤集合
        /// </summary>
        /// <param name="flowRunModel"></param>
        /// <param name="stepId"></param>
        /// <returns></returns>
        public List<Model.FlowRunModel.Step> GetPrevSteps(Model.FlowRun flowRunModel, Guid stepId)
        {
            List<Model.FlowRunModel.Step> steps = new List<Model.FlowRunModel.Step>();
            if (null == flowRunModel || stepId.IsEmptyGuid())
            {
                return steps;
            }
            var lines = flowRunModel.Lines.FindAll(p => p.ToId == stepId);
            foreach (var line in lines)
            {
                var step = flowRunModel.Steps.Find(p => p.Id == line.FromId);
                if (null != step)
                {
                    steps.Add(step);
                }
            }
            return steps;
        }

        #region 得到两个步骤之间的步骤集合
        /// <summary>
        /// 得到两个步骤之间的步骤集合
        /// </summary>
        /// <param name="flowRunModel"></param>
        /// <param name="stepId1">开始步骤Id</param>
        /// <param name="stepId2">结束步骤Id</param>
        /// <returns></returns>
        public List<Model.FlowRunModel.Step> GetRangeSteps(Model.FlowRun flowRunModel, Guid stepId1, Guid stepId2)
        {
            List<List<Model.FlowRunModel.Step>> stepsList = new List<List<Model.FlowRunModel.Step>>();
            AddRangeSteps(stepsList, flowRunModel, stepId1, stepId2);
            List<Model.FlowRunModel.Step> steps1 = new List<Model.FlowRunModel.Step>();
            foreach (var steps in stepsList)
            {
                if (steps.Exists(p => p.Id == stepId2))
                {
                    foreach (var step in steps)
                    {
                        if (step.Id == stepId2)
                        {
                            break;
                        }
                        steps1.Add(step);
                    } 
                }
            }
            return steps1.Distinct().ToList();
        }
        private void AddRangeSteps(List<List<Model.FlowRunModel.Step>> steps, Model.FlowRun runModel, Guid stepId1, Guid stepId2)
        {
            var flowSteps = runModel.Steps;
            var lines = runModel.Lines.FindAll(p => p.FromId == stepId1);
            foreach (var line in lines)
            {
                var step = flowSteps.Find(p => p.Id == line.ToId);
                if (null != step)
                {
                    List<Model.FlowRunModel.Step> steps1 = new List<Model.FlowRunModel.Step>();
                    steps1.Add(step);
                    AddRangeSteps1(steps1, runModel, step.Id, stepId2);
                    steps.Add(steps1.Distinct().ToList());
                }
            }
        }
        private void AddRangeSteps1(List<Model.FlowRunModel.Step> steps, Model.FlowRun runModel, Guid stepId1, Guid stepId2)
        {
            var flowSteps = runModel.Steps;
            var lines = runModel.Lines.FindAll(p => p.FromId == stepId1);
            foreach (var line in lines)
            {
                var step = flowSteps.Find(p => p.Id == line.ToId);
                if (null != step)
                {
                    steps.Add(step);
                    AddRangeSteps1(steps, runModel, step.Id, stepId2);
                }
            }
        }
        #endregion

        /// <summary>
        /// 得到一个流程步骤的下一步骤
        /// </summary>
        /// <param name="flowId">流程ID</param>
        /// <param name="stepId">步骤ID</param>
        /// <returns></returns>
        public List<Model.FlowRunModel.Step> GetNextSteps(Guid flowId, Guid stepId)
        {
            return GetNextSteps(GetFlowRunModel(flowId), stepId);
        }
        /// <summary>
        /// 根据ID得到流程名称
        /// </summary>
        /// <param name="flowId"></param>
        /// <returns></returns>
        public string GetName(Guid flowId)
        {
            if (flowId.IsEmptyGuid())
            {
                return string.Empty;
            }
            var flow = Get(flowId);
            return null == flow ? string.Empty : flow.Name;
        }

        /// <summary>
        /// 得到步骤名称
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="stepId"></param>
        /// <returns></returns>
        public string GetStepName(Guid flowId, Guid stepId)
        {
            return flowId.IsEmptyGuid() ? string.Empty : GetStepName(GetFlowRunModel(flowId), stepId);
        }

        /// <summary>
        /// 得到步骤名称
        /// </summary>
        /// <param name="flowId"></param>
        /// <param name="stepId"></param>
        /// <returns></returns>
        public string GetStepName(Model.FlowRun flowRunModel, Guid stepId)
        {
            if (stepId.IsEmptyGuid())
            {
                return string.Empty;
            }
            if (null == flowRunModel)
            {
                return string.Empty;
            }
            var step = flowRunModel.Steps.Find(p => p.Id == stepId);
            return null == step ? string.Empty : step.Name;
        }

        /// <summary>
        /// 得到一个用户可管理的流程
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Model.Flow> GetManageFlow(Guid userId)
        {
            var all = GetAll();
            if (!all.Any())
            {
                return new List<Model.Flow>();
            }
            return all.FindAll(p => p.Manager.EqualsIgnoreCase(Organize.PREFIX_USER + userId.ToString()));
        }

        /// <summary>
        /// 得到一个用户可管理的流程
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Guid> GetManageFlowIds(Guid userId)
        {
            var flows = GetManageFlow(userId);
            List<Guid> guids = new List<Guid>();
            foreach (var flow in flows)
            {
                guids.Add(flow.Id);
            }
            return guids;
        }

        /// <summary>
        /// 得到一个用户可管理实例的流程
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Model.Flow> GetManageInstanceFlow(Guid userId)
        {
            var all = GetAll();
            if (!all.Any())
            {
                return new List<Model.Flow>();
            }
            return all.FindAll(p => p.InstanceManager.EqualsIgnoreCase(Organize.PREFIX_USER + userId.ToString()));
        }

        /// <summary>
        /// 得到一个用户可管理实例的流程
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Guid> GetManageInstanceFlowIds(Guid userId)
        {
            var flows = GetManageInstanceFlow(userId);
            List<Guid> guids = new List<Guid>();
            foreach (var flow in flows)
            {
                guids.Add(flow.Id);
            }
            return guids;
        }

        /// <summary>
        /// 得到一个用户可以发起的流程运行时实体列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Model.FlowRun> GetStartFlows(Guid userId)
        {
            List<Model.FlowRun> flowRuns = new List<Model.FlowRun>();
            var flows = GetAll();
            User user = new User();
            foreach(var flow in flows)
            {
                var flowRunModel = GetFlowRunModel(flow.Id);
                if (null == flowRunModel || flowRunModel.Status != 1 || flowRunModel.FirstStepId.IsEmptyGuid())
                {
                    continue;
                }
                var firstStepModel = flowRunModel.Steps.Find(p => p.Id == flowRunModel.FirstStepId);
                if (null == firstStepModel)
                {
                    continue;
                }
                if (firstStepModel.StepBase.DefaultHandler.IsNullOrWhiteSpace() || user.Contains(firstStepModel.StepBase.DefaultHandler, userId))
                {
                    flowRuns.Add(flowRunModel);
                }
            }
            return flowRuns;
        }

        /// <summary>
        /// 得到导出时的流程JSON字符串
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public string GetExportFlowString(string ids)
        {
            if(ids.IsNullOrWhiteSpace())
            {
                return "";
            }
            string[] flowIds = ids.Split(',');
            JObject jObject = new JObject();
            JArray jArrayFlow = new JArray();
            JArray jArrayForm = new JArray();
            JArray jArrayApplibrary = new JArray();
            List<Guid> formIdList = new List<Guid>();//用来保存已添加的表单，避免重复
            List<Guid> applibraryIdList = new List<Guid>();//用来保存已添加的应用程序库，避免重复
            foreach (string flowId in flowIds)
            {
                if (!flowId.IsGuid(out Guid fid))
                {
                    continue;
                }
                var flowModel = Get(fid);
                if (null == flowModel)
                {
                    continue;
                }
                jArrayFlow.Add(JObject.FromObject(flowModel));
                //添加表单和应用程序库
                var flowRunModel = GetFlowRunModel(fid);
                if (flowModel.RunJSON.IsNullOrWhiteSpace() || flowModel.DesignerJSON.IsNullOrWhiteSpace())
                {
                    continue;
                }
                JObject flowObject = null;
                try
                {
                    flowObject = JObject.Parse(flowModel.RunJSON.IsNullOrWhiteSpace() ? flowModel.DesignerJSON : flowModel.RunJSON);
                }
                catch
                {

                }
                if (flowObject == null)
                {
                    continue;
                }
                JArray stepArray = flowObject.Value<JArray>("steps");
                if (stepArray == null)
                {
                    continue;
                }
                AppLibrary appLibrary = new AppLibrary();
                Form form = new Form();
                foreach (JObject stepObject in stepArray)
                {
                    JArray formArray = stepObject.Value<JArray>("forms");
                    JObject formObject = (JObject)formArray.First;
                    //PC表单
                    if (formObject.Value<string>("id").IsGuid(out Guid formId))
                    {
                        var appModel = appLibrary.Get(formId);
                        if (appModel != null)
                        {
                            if (!applibraryIdList.Contains(appModel.Id))
                            {
                                jArrayApplibrary.Add(JObject.FromObject(appModel));
                                applibraryIdList.Add(appModel.Id);
                            }
                            if (!appModel.Code.IsNullOrWhiteSpace() && appModel.Code.IsGuid(out Guid formGuidId))
                            {
                                var formModel = form.Get(formGuidId);
                                if (formModel != null)
                                {
                                    if (!formIdList.Contains(formModel.Id))
                                    {
                                        jArrayForm.Add(JObject.FromObject(formModel));
                                        formIdList.Add(formModel.Id);
                                    }
                                }
                            }
                        }
                    }
                    //移动端表单
                    if (formObject.Value<string>("idApp").IsGuid(out Guid appId))
                    {
                        var appModel = appLibrary.Get(appId);
                        if (appModel != null)
                        {
                            if (!applibraryIdList.Contains(appModel.Id))
                            {
                                jArrayApplibrary.Add(JObject.FromObject(appModel));
                                applibraryIdList.Add(appModel.Id);
                            }
                            if (!appModel.Code.IsNullOrWhiteSpace() && appModel.Code.IsGuid(out Guid formGuidId))
                            {
                                var formModel = form.Get(formGuidId);
                                if (formModel != null)
                                {
                                    if (!formIdList.Contains(formModel.Id))
                                    {
                                        jArrayForm.Add(JObject.FromObject(formModel));
                                        formIdList.Add(formModel.Id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            jObject.Add("flows", jArrayFlow);
            jObject.Add("forms", jArrayForm);
            jObject.Add("applibrarys", jArrayApplibrary);
            return jObject.ToString();
        }

        /// <summary>
        /// 导入流程
        /// </summary>
        /// <param name="json"></param>
        /// <returns>返回1表示成功，其它为错误信息</returns>
        public string ImportFlow(string json)
        {
            if (json.IsNullOrWhiteSpace())
            {
                return "要导入的JSON为空!";
            }
            JObject jObject = null;
            try
            {
                jObject = JObject.Parse(json);
            }
            catch
            {
                return "json解析错误!";
            }
            var flows = jObject.Value<JArray>("flows");
            if (null != flows)
            {
                foreach (JObject flow in flows)
                {
                    Model.Flow flowModel = flow.ToObject<Model.Flow>();
                    if (null == flowModel)
                    {
                        continue;
                    }
                    if (Get(flowModel.Id) != null)
                    {
                        Update(flowModel);
                    }
                    else
                    {
                        Add(flowModel);
                    }
                }
            }
            var applibarys = jObject.Value<JArray>("applibrarys");
            AppLibrary appLibrary = new AppLibrary();
            if (null != applibarys)
            {
                foreach (JObject app in applibarys)
                {
                    Model.AppLibrary appLibraryModel = app.ToObject<Model.AppLibrary>();
                    if (null == appLibraryModel)
                    {
                        continue;
                    }
                    if (appLibrary.Get(appLibraryModel.Id) != null)
                    {
                        appLibrary.Update(appLibraryModel);
                    }
                    else
                    {
                        appLibrary.Add(appLibraryModel);
                    }
                }
            }
            var forms = jObject.Value<JArray>("forms");
            Form bform = new Form();
            if (null != forms)
            {
                foreach (JObject form in forms)
                {
                    Model.Form formModel = form.ToObject<Model.Form>();
                    if (null == formModel)
                    {
                        continue;
                    }
                    if (bform.Get(formModel.Id) != null)
                    {
                        bform.Update(formModel);
                    }
                    else
                    {
                        bform.Add(formModel);
                    }
                    //如果表单状态是发布，要发布表单
                    if (formModel.Status == 1)
                    {
                        #region 写入文件
                        string webRootPath = Tools.GetWebRootPath();
                        string path = webRootPath + "/RoadFlowResources/scripts/formDesigner/form/";
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string file = path + formModel.Id + ".rfhtml";
                        Stream stream = File.Open(file, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                        stream.SetLength(0);
                        StreamWriter sw = new StreamWriter(stream, Encoding.UTF8);
                        sw.Write(formModel.RunHtml);
                        sw.Close();
                        stream.Close();
                        #endregion
                    }
                }
            }
            return "1";
        }
    }
}
