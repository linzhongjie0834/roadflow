﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RoadFlow.Model
{
  [Table("RF_MailContent")]
  [Serializable]
  public class MailContent
  {
    [Key]
    [Required(ErrorMessage = "Id不能为空")]
    [Column("Id")]
    [DisplayName("Id")]
    public Guid Id { get; set; }

    [Required(ErrorMessage = "邮件内容不能为空")]
    [Column("Contents")]
    [DisplayName("邮件内容")]
    public string Contents { get; set; }

    [Column("Files")]
    [DisplayName("附件")]
    public string Files { get; set; }

    public override string ToString()
    {
      return JsonConvert.SerializeObject((object) this);
    }
  }
}
