﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using RoadFlow.Utility;

namespace RoadFlow.Business
{
    public class Log
    {
        public enum Type
        {
            用户登录,
            系统管理,
            流程管理,
            表单管理,
            流程运行,
            其它
        }
        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="log"></param>
        private static void AddLog(Model.Log log)
        {
            new Data.Log().Add(log);
        }

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="log"></param>
        public static void Add(Model.Log log)
        {
            Task task = Task.Run(() => AddLog(log));
        }

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="title"></param>
        /// <param name="contents"></param>
        /// <param name="type"></param>
        /// <param name="oldContents"></param>
        /// <param name="newContents"></param>
        /// <param name="others"></param>
        /// <param name="browseAgent"></param>
        /// <param name="ipAddress"></param>
        /// <param name="url"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        public static void Add(string title, string contents = "", Type type = Type.其它, string oldContents = "", string newContents = "", string others = "", string browseAgent = "", string ipAddress = "", string url = "", string userId = "", string userName = "")
        {
            Model.Log logModel = new Model.Log
            {
                Id = Guid.NewGuid(),
                Title = title,
                Type = type.ToString(),
                IPAddress = ipAddress.IsNullOrWhiteSpace() ? Tools.GetIP() : ipAddress,
                URL = url.IsNullOrWhiteSpace() ? Tools.GetAbsoluteURL() : url,
                WriteTime = DateExtensions.Now,
                Referer = Tools.GetReferer()
            };
            if (userId.IsGuid(out Guid userGuid))
            {
                logModel.UserId = userGuid;
            }
            else
            {
                Guid uid = User.CurrentUserId;
                if (uid.IsEmptyGuid())
                {
                    uid = EnterpriseWeiXin.Common.GetUserId();
                }
                if (uid.IsNotEmptyGuid())
                {
                    logModel.UserId = uid;
                }
            }
            if (!userName.IsNullOrWhiteSpace())
            {
                logModel.UserName = userName;
            }
            else
            {
                var userModel = User.CurrentUser;
                if (null == userModel)
                {
                    userModel = EnterpriseWeiXin.Common.GetUser();
                }
                if (null != userModel)
                {
                    logModel.UserName = userModel.Name;
                }
            }
            if (!contents.IsNullOrWhiteSpace())
            {
                logModel.Contents = contents;
            }
            if (!others.IsNullOrWhiteSpace())
            {
                logModel.Others = others;
            }
            if (!oldContents.IsNullOrWhiteSpace())
            {
                logModel.OldContents = oldContents;
            }
            if (!newContents.IsNullOrWhiteSpace())
            {
                logModel.NewContents = newContents;
            }
            logModel.BrowseAgent = browseAgent.IsNullOrWhiteSpace() ? Tools.GetBrowseAgent() : browseAgent;
            AddLog(logModel);
        }

        /// <summary>
        /// 查询一页日志
        /// </summary>
        /// <param name="count"></param>
        /// <param name="size"></param>
        /// <param name="number"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderbyLambda"></param>
        /// <param name="isAsc"></param>
        /// <returns></returns>
        public System.Data.DataTable GetPagerList(out int count, int size, int number, string title, string type, string userId, string date1, string date2, string order)
        {
            return new Data.Log().GetPagerList(out count, size, number, title, type, userId, date1, date2, order);
        }

        /// <summary>
        /// 查询一条日志
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Log Get(Guid id)
        {
            return new Data.Log().Get(id);
        }

        /// <summary>
        /// 得到类别下拉项
        /// </summary>
        /// <returns></returns>
        public string GetTypeOptions(string value = "")
        {
            StringBuilder options = new StringBuilder();
            var array = Enum.GetValues(typeof(Type));
            foreach (var arr in array)
            {
                options.AppendFormat("<option value=\"{0}\" {1}>{0}</option>", arr, arr.ToString().Equals(value) ? "selected=\"selected\"" : "");
            }
            return options.ToString();
        }
    }
} 
