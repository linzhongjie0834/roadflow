﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoadFlow.Integrate
{
    public class Organize
    {
        /// <summary>
        /// 得到系统所有用户
        /// </summary>
        /// <returns></returns>
        public List<Model.User> GetAllUser()
        {
            return new Data.User().GetAll();
        }

        /// <summary>
        /// 得到系统所有组织架构
        /// </summary>
        /// <returns></returns>
        public List<Model.Organize> GetAllOrganize()
        {
            return new Data.Organize().GetAll();
        }

        /// <summary>
        /// 得到所有组织架构与人员关系
        /// </summary>
        /// <returns></returns>
        public List<Model.OrganizeUser> GetAllOrganizeUser()
        {
            return new Data.OrganizeUser().GetAll();
        }

        /// <summary>
        /// 得到所有工作组/角色组
        /// </summary>
        /// <returns></returns>
        public List<Model.WorkGroup> GetAllWorkGroup()
        {
            return new Data.WorkGroup().GetAll();
        }
    }
}
