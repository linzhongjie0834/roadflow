﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoadFlow.Business
{


    public class Area
    {
        readonly Data.Area area;

        public Area()
        {

            area = new Data.Area();

        }

        public List<Model.Area> GetAll()
        {
            return area.GetAll();
        }

        public Model.Area Get(Guid id)
        {
            return area.GetAll().Find(p => p.Id == id);
        }

        public int Add(Model.Area item)
        {
            return area.Add(item); 
        }

        public int Update(Model.Area item)
        {
            return area.Update(item);
        }

        public int Delete(Model.Area[] items)
        {
            return area.Delete(items);
        }

    }
}
